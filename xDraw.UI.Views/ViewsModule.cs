﻿using Autofac;
using xDraw.UI.Framework;
using xDraw.UI.Views.Main;

namespace xDraw.UI.Views
{
	public class ViewsModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			builder.RegisterType<MainWindow>().As<IMainWindow>().SingleInstance();
		}
	}
}