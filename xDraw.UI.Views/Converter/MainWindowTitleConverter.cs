﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace xDraw.UI.Views.Converter
{
	[ValueConversion(typeof(string), typeof(string))]
	public class MainWindowTitleConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value == null) return null;
			string title = value.ToString();
			if (!string.IsNullOrEmpty(title))
				title += " - ";
			return title;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}