﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Windows.Data;
using System.Windows.Markup;

namespace xDraw.UI.Views.Converter
{
	[ContentProperty("Converters")]
	[ContentWrapper(typeof(Collection<IValueConverter>))]
	public class ChainConverters : IValueConverter
	{
		public Collection<IValueConverter> Converters { get; set; } = new Collection<IValueConverter>();

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return Converters.Aggregate(value,
				(current, converter) => converter.Convert(current, targetType, parameter, culture));
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return Converters.Aggregate(value,
				(current, converter) => converter.ConvertBack(current, targetType, parameter, culture));
		}
	}
}