﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace xDraw.UI.Views.Extensions
{
	public static class FocusExtension
	{
		public static readonly DependencyProperty IsFocusedProperty =
			DependencyProperty.RegisterAttached(
				"IsFocused", typeof(bool), typeof(FocusExtension),
				new UIPropertyMetadata(false, OnIsFocusedPropertyChanged));

		public static bool GetIsFocused(DependencyObject obj)
		{
			return (bool) obj.GetValue(IsFocusedProperty);
		}


		public static void SetIsFocused(DependencyObject obj, bool value)
		{
			obj.SetValue(IsFocusedProperty, value);
		}


		private static void OnIsFocusedPropertyChanged(DependencyObject d,
			DependencyPropertyChangedEventArgs e)
		{
			var uie = (UIElement) d;
			if (!(bool) e.NewValue) return;
			uie.Focus();
			Keyboard.Focus(uie);
			if (!(uie is TextBox)) return;
			var tbUi = uie as TextBox;
			tbUi.SelectAll();
		}
	}
}