﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using xDraw.UI.Framework;

namespace xDraw.UI.Views.Main
{
    public partial class MainWindow : IMainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            Application.Current.MainWindow = this;
        }

        private void StartLayerRearrange(object sender, MouseEventArgs e)
        {
            if (sender is ListBoxItem draggedItem && e.LeftButton == MouseButtonState.Pressed)
            {
                DragDrop.DoDragDrop(draggedItem, draggedItem.DataContext, DragDropEffects.Move);
            }
        }

        private void Selector_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}