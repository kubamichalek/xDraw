﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using Mathtone.MIST;

namespace xDraw.Common
{
	public class NotifierBase : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		[NotifyTarget]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}