﻿using System.Windows;
using System.Windows.Input;

namespace xDraw.Common.EventArgs
{
    public delegate void MouseWithLocaionEventHandler(object sender, MouseWithLocationEventArgs e);

    public class MouseWithLocationEventArgs : MouseEventArgs
    {
        private readonly IInputElement _inputElement;

        public MouseWithLocationEventArgs(IInputElement inputElement, MouseEventArgs e) : base(e.MouseDevice, e.Timestamp, e.StylusDevice)
        {
            _inputElement = inputElement;
        }

        public Point Position => GetPosition(_inputElement);
    }
}