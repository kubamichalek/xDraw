﻿using System.Runtime.CompilerServices;
using GalaSoft.MvvmLight;
using Mathtone.MIST;

namespace xDraw.Common
{
	public class ViewModelNotifierBase : ViewModelBase
	{
		[NotifyTarget]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			RaisePropertyChanged(propertyName);
		}
	}
}