﻿using System;
using System.Windows;

namespace xDraw.StandardFilters
{
    internal interface IMapFilter
    {
        IObservable<double> Progress { get; }

        unsafe void ApplyMap(int* pixels, Size size, bool ignoreAlpha = false);
    }
}
