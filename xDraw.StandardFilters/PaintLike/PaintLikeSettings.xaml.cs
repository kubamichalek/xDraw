﻿using System.Windows;

namespace xDraw.StandardFilters.PaintLike
{

    public partial class PaintLikeSettings
    {
        public PaintLikeSettings()
        {
            DataContext = this;
            Distance = 1;
            Algorithm = PaintLikeAlgorithms.Fast;
            InitializeComponent();
        }

        public int Distance { get; set; }
        public bool IsFast
        {
            get => Algorithm == PaintLikeAlgorithms.Fast;
            set
            {
                if (value)
                    Algorithm = PaintLikeAlgorithms.Fast;
            }
        }
        public bool IsKuwahara
        {
            get => Algorithm == PaintLikeAlgorithms.Kuwahara;
            set
            {
                if (value)
                    Algorithm = PaintLikeAlgorithms.Kuwahara;
            }
        }
        internal PaintLikeAlgorithms Algorithm { get; private set; }

        private void OkClicked(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }
    }
}
