﻿using System;
using System.Reactive.Subjects;
using System.Windows;

namespace xDraw.StandardFilters
{
    internal abstract class BaseMapFilter : IMapFilter
    {
        private readonly Subject<double> _progress = new Subject<double>();
        public IObservable<double> Progress => _progress;

        protected void OnProgress(double percentage)
        {
            Application.Current.Dispatcher.Invoke(() =>
                _progress.OnNext(percentage)
            );

        }

        public abstract unsafe void ApplyMap(int* pixels, Size size, bool ignoreAlpha = false);
    }
}
