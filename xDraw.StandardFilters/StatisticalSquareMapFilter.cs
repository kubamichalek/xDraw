﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Windows;

namespace xDraw.StandardFilters
{
    internal class StatisticalSquareMapFilter : BaseMapFilter
    {
        public enum StatFunction
        {
            Median,
            Minimum
        }

        private readonly int _distance;
        private readonly StatFunction _function;

        public StatisticalSquareMapFilter(int distance, StatFunction function)
        {
            _distance = distance;
            _function = function;
        }

        public override unsafe void ApplyMap(int* pixels, Size size, bool ignoreAlpha = false)
        {
            var pxWidth = (int)size.Width;
            var pxHeight = (int)size.Height;
            var tmpBuf = new int[pxWidth * pxHeight];
            int mapWidth = 2 * _distance + 1;
            var mapABuf = new int[mapWidth * mapWidth];
            var mapRBuf = new int[mapWidth * mapWidth];
            var mapGBuf = new int[mapWidth * mapWidth];
            var mapBBuf = new int[mapWidth * mapWidth];

            for (var x = 0; x < pxWidth; x++)
            {
                OnProgress(x * 100.0 / pxWidth);
                for (var y = 0; y < pxHeight; y++)
                {
                    for (int mx = -_distance; mx <= _distance; mx++)
                    {
                        for (int my = -_distance; my <= _distance; my++)
                        {
                            if (x + mx < 0 || x + mx >= pxWidth ||
                                y + my < 0 || y + my >= pxHeight)
                            {
                                mapABuf[(my + _distance) * mapWidth + mx + _distance] = -1;
                                mapRBuf[(my + _distance) * mapWidth + mx + _distance] = -1;
                                mapGBuf[(my + _distance) * mapWidth + mx + _distance] = -1;
                                mapBBuf[(my + _distance) * mapWidth + mx + _distance] = -1;
                                continue;
                            }
                            int pixel = pixels[(y + my) * pxWidth + x + mx];
                            int sA = pixel >> 24 & byte.MaxValue;
                            int sR = pixel >> 16 & byte.MaxValue;
                            int sG = pixel >> 8 & byte.MaxValue;
                            int sB = pixel & byte.MaxValue;

                            mapABuf[(my + _distance) * mapWidth + mx + _distance] = sA;
                            mapRBuf[(my + _distance) * mapWidth + mx + _distance] = sR;
                            mapGBuf[(my + _distance) * mapWidth + mx + _distance] = sG;
                            mapBBuf[(my + _distance) * mapWidth + mx + _distance] = sB;
                        }
                    }

                    var dA = 0;
                    var dR = 0;
                    var dG = 0;
                    var dB = 0;

                    IEnumerable<int> filteredMapABuf = mapABuf.SkipWhile(v => v < 0);
                    IEnumerable<int> filteredMapRBuf = mapRBuf.SkipWhile(v => v < 0);
                    IEnumerable<int> filteredMapGBuf = mapGBuf.SkipWhile(v => v < 0);
                    IEnumerable<int> filteredMapBBuf = mapBBuf.SkipWhile(v => v < 0);
                    switch (_function)
                    {
                        case StatFunction.Median:
                            dA = ignoreAlpha ? mapABuf[mapABuf.Length / 2 + 1] : FindMedian(filteredMapABuf.ToArray());
                            dR = FindMedian(filteredMapRBuf.ToArray());
                            dG = FindMedian(filteredMapGBuf.ToArray());
                            dB = FindMedian(filteredMapBBuf.ToArray());
                            break;
                        case StatFunction.Minimum:
                            dA = ignoreAlpha ? mapABuf[mapABuf.Length / 2 + 1] : FindMinimum(filteredMapABuf);
                            dR = FindMinimum(filteredMapRBuf);
                            dG = FindMinimum(filteredMapGBuf);
                            dB = FindMinimum(filteredMapBBuf);
                            break;
                    }

                    dA = dA < 0 ? 0 :
                        dA > 255 ? 255 : dA;
                    dR = dR < 0 ? 0 :
                        dR > 255 ? 255 : dR;
                    dR = ((dR + 1) * dA) >> 8;
                    dG = dG < 0 ? 0 :
                        dG > 255 ? 255 : dG;
                    dG = ((dG + 1) * dA) >> 8;
                    dB = dB < 0 ? 0 :
                        dB > 255 ? 255 : dB;
                    dB = ((dB + 1) * dA) >> 8;

                    tmpBuf[y * pxWidth + x] = dA << 24 | dR << 16 | dG << 8 | dB;
                }
            }

            Marshal.Copy(tmpBuf, 0, new IntPtr(pixels), tmpBuf.Length);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static int FindMinimum(IEnumerable<int> values)
        {
            return values.Where(v => v >= 0).Min();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static int FindMedian(int[] values)
        {
            Array.Sort(values);
            int arLen = values.Length;

            return values[arLen / 2 + 1];
        }
    }
}
