﻿namespace xDraw.StandardFilters.Grayscale
{
    internal enum GrayscaleAlgorithms
    {
        Average,
        Lightness,
        Luminosity
    }
}
