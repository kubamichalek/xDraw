﻿using System.Windows;

namespace xDraw.StandardFilters.Grayscale
{

    public partial class GrayscaleSettings
    {
        public GrayscaleSettings()
        {
            DataContext = this;
            Algorithm = GrayscaleAlgorithms.Average;
            InitializeComponent();
        }

        public bool NormalAlgorithm
        {
            get => Algorithm == GrayscaleAlgorithms.Average;
            set
            {
                if (value)
                    Algorithm = GrayscaleAlgorithms.Average;
            }
        }

        public bool LightnessAlgorithm
        {
            get => Algorithm == GrayscaleAlgorithms.Lightness;
            set
            {
                if (value)
                    Algorithm = GrayscaleAlgorithms.Lightness;
            }
        }

        public bool LuminosityAlgorithm
        {
            get => Algorithm == GrayscaleAlgorithms.Luminosity;
            set
            {
                if (value)
                    Algorithm = GrayscaleAlgorithms.Luminosity;
            }
        }

        internal GrayscaleAlgorithms Algorithm { get; private set; }

        private void OkClicked(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }
    }
}
