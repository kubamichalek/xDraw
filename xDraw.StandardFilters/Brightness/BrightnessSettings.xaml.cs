﻿using System.Windows;

namespace xDraw.StandardFilters.Brightness
{

    public partial class BrightnessSettings
    {
        public BrightnessSettings()
        {
            DataContext = this;
            InitializeComponent();
        }

        public int Delta { get; set; }

        private void OkClicked(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }
    }
}
