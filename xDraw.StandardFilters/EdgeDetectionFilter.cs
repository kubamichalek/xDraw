﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Media;
using xDraw.Common.Modals;
using xDraw.Logic.Filters;

namespace xDraw.StandardFilters
{
	internal class EdgeDetectionFilter : BaseFilter
	{
		public EdgeDetectionFilter(IFilterManager filterManager) : base(filterManager)
		{
		}

		public override string Name => "Wykrywanie krawędzi";

		public override unsafe void Execute(int* pixels, Size size, PixelFormat format)
		{
			var map = new[]
			{
				0, -1, 0,
				-1, 4, -1,
				0, -1, 0
			};
			var filter = new WeightedMeanSquareMapFilter(map);
			var worker = new BackgroundWorker();
			var progressBar = new ProgressModal
			{
				Title = Name,
				Description = "Nakładanie filtra w trakcie..."
			};
			filter.Progress.Subscribe(v => progressBar.ProgressValue = v);
			worker.DoWork += (sender, args) =>
			{
				filter.ApplyMap(pixels, size, ignoreAlpha: true);
			};
			worker.RunWorkerCompleted += (sender, args) => progressBar.Close();
			worker.RunWorkerAsync();
			progressBar.ShowDialog();
		}
	}
}
