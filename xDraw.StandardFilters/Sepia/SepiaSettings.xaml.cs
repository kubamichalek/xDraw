﻿using System.Windows;

namespace xDraw.StandardFilters.Sepia
{

    public partial class SepiaSettings
    {
        public SepiaSettings()
        {
            DataContext = this;
            Intensivity = 100;
            InitializeComponent();
        }

        public int Intensivity { get; set; }

        private void OkClicked(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }
    }
}
