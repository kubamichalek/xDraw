﻿using System.Windows;
using System.Windows.Media;
using xDraw.Logic.Filters;
using xDraw.StandardFilters.Sepia;

namespace xDraw.StandardFilters
{
	internal class SepiaFilter : BaseFilter
	{
		public SepiaFilter(IFilterManager filterManager) : base(filterManager)
		{
		}

		public override string Name => "Sepia";

		public override unsafe void Execute(int* pixels, Size size, PixelFormat format)
		{
			var settings = new SepiaSettings();
			if (settings.ShowDialog().GetValueOrDefault())
			{
				float intensivity = 1 - settings.Intensivity / 100f;
				var length = (int)(size.Width * size.Height);
				for (var i = 0; i < length; i++)
				{
					int c = pixels[i];
					int sA = c >> 24 & byte.MaxValue << 24;
					int sR = c >> 16 & byte.MaxValue;
					int sG = c >> 8 & byte.MaxValue;
					int sB = c & byte.MaxValue;

					var dR = (int)(sR * .393 + sG * .769 + sB * .189);
					dR = dR > 255 ? 255 : dR;
					int distance = dR - sR;
					dR -= (int)(distance * intensivity);
					dR <<= 16;
					var dG = (int)(sR * .349 + sG * .686 + sB * .168);
					dG = dG > 255 ? 255 : dG;
					distance = dG - sG;
					dG -= (int)(distance * intensivity);
					dG <<= 8;
					var dB = (int)(sR * .272 + sG * .534 + sB * .131);
					dB = dB > 255 ? 255 : dB;
					distance = dB - sB;
					dB -= (int)(distance * intensivity);

					pixels[i] = sA | dR | dG | dB;
				}
			}
		}
	}
}
