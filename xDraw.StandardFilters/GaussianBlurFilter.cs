﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Media;
using xDraw.Common.Modals;
using xDraw.Logic.Filters;

namespace xDraw.StandardFilters
{
	internal class GaussianBlurFilter : BaseFilter
	{
		public GaussianBlurFilter(IFilterManager filterManager) : base(filterManager)
		{
		}

		public override string Name => "Rozmycie Gaussa";

		public override unsafe void Execute(int* pixels, Size size, PixelFormat format)
		{
			var weightMap = new[]
			{
				1, 1, 2, 1, 1,
				1, 2, 4, 2, 1,
				2, 4, 8, 4, 2,
				1, 2, 4, 2, 1,
				1, 1, 2, 1, 1
			};
			var filter = new WeightedMeanSquareMapFilter(weightMap);
			var worker = new BackgroundWorker();
			var progressBar = new ProgressModal
			{
				Title = Name,
				Description = "Nakładanie filtra w trakcie..."
			};
			filter.Progress.Subscribe(v => progressBar.ProgressValue = v);
			worker.DoWork += (sender, args) =>
			{
				filter.ApplyMap(pixels, size);
			};
			worker.RunWorkerCompleted += (sender, args) => progressBar.Close();
			worker.RunWorkerAsync();
			progressBar.ShowDialog();
		}
	}
}
