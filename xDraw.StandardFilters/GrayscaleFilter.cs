﻿using System;
using System.Windows;
using System.Windows.Media;
using xDraw.Logic.Filters;
using xDraw.StandardFilters.Grayscale;

namespace xDraw.StandardFilters
{
	internal class GrayscaleFilter : BaseFilter
	{
		public GrayscaleFilter(IFilterManager filterManager) : base(filterManager)
		{
		}

		public override string Name => "Skala szarości";

		public override unsafe void Execute(int* pixels, Size size, PixelFormat format)
		{
			var settings = new GrayscaleSettings();
			if (settings.ShowDialog().GetValueOrDefault())
			{
				GrayscaleAlgorithms algorithm = settings.Algorithm;
				var length = (int)(size.Width * size.Height);
				for (var i = 0; i < length; i++)
				{
					int c = pixels[i];
					int sA = c >> 24 & byte.MaxValue;
					int sR = c >> 16 & byte.MaxValue;
					int sG = c >> 8 & byte.MaxValue;
					int sB = c & byte.MaxValue;
					int dR, dG, dB;
					switch (algorithm)
					{
						case GrayscaleAlgorithms.Average:
							int avg = (sR + sG + sB) / 3;
							dR = dG = dB = avg;
							break;
						case GrayscaleAlgorithms.Lightness:
							avg = (Math.Max(Math.Max(sR, sG), sB) + Math.Min(Math.Min(sR, sG), sB)) / 2;
							dR = dG = dB = avg;
							break;
						case GrayscaleAlgorithms.Luminosity:
							avg = (int)(sR * 0.21 + sG * 0.72 + sB * 0.07);
							dR = dG = dB = avg;
							break;
						default:
							throw new ArgumentOutOfRangeException(nameof(algorithm));
					}

					pixels[i] = sA << 24 | dR << 16 | dG << 8 | dB;
				}
			}
		}
	}
}
