﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Media;
using xDraw.Common.Modals;
using xDraw.Logic.Filters;
using xDraw.StandardFilters.PaintLike;

namespace xDraw.StandardFilters
{
	internal class PaintLikeFilter : BaseFilter
	{
		public PaintLikeFilter(IFilterManager filterManager) : base(filterManager)
		{
		}

		public override string Name => "Obraz olejny";

		public override unsafe void Execute(int* pixels, Size size, PixelFormat format)
		{
			var settings = new PaintLikeSettings();
			if (settings.ShowDialog().GetValueOrDefault())
			{
				IMapFilter filter;
				switch (settings.Algorithm)
				{
					case PaintLikeAlgorithms.Fast:
						filter = new StatisticalSquareMapFilter(settings.Distance, StatisticalSquareMapFilter.StatFunction.Minimum);
						break;
					case PaintLikeAlgorithms.Kuwahara:
						filter = new KuwaharaFilter(settings.Distance);
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}

				ExecuteFilter(filter, pixels, size);
			}
		}

		private unsafe void ExecuteFilter(IMapFilter filter, int* pixels, Size size)
		{
			var worker = new BackgroundWorker();
			var progressBar = new ProgressModal
			{
				Title = Name,
				Description = "Nakładanie filtra w trakcie..."
			};
			filter.Progress.Subscribe(v => progressBar.ProgressValue = v);
			worker.DoWork += (sender, args) =>
			{
				filter.ApplyMap(pixels, size);
			};
			worker.RunWorkerCompleted += (sender, args) => progressBar.Close();
			worker.RunWorkerAsync();
			progressBar.ShowDialog();
		}
	}
}