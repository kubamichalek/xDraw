﻿using System.Windows;
using System.Windows.Media;
using xDraw.Logic.Filters;
using xDraw.StandardFilters.Contrast;

namespace xDraw.StandardFilters
{
	internal class ContrastFilter : BaseFilter
	{
		public ContrastFilter(IFilterManager filterManager) : base(filterManager)
		{
		}

		public override string Name => "Kontrast";

		public override unsafe void Execute(int* pixels, Size size, PixelFormat format)
		{
			var settings = new ConstrastSettings();
			if (settings.ShowDialog().GetValueOrDefault())
			{
				var delta = (int)(settings.Delta / 100f * 255);
				var factor = (int)((259.0 * (delta + 255.0)) / (255.0 * (259.0 - delta)) * 255.0);
				var length = (int)(size.Width * size.Height);
				for (var i = 0; i < length; i++)
				{
					int c = pixels[i];
					int sA = c >> 24 & byte.MaxValue;
					int sR = c >> 16 & byte.MaxValue;
					int sG = c >> 8 & byte.MaxValue;
					int sB = c & byte.MaxValue;

					sR = ((factor * (sR - 128)) >> 8) + 128;
					sG = ((factor * (sB - 128)) >> 8) + 128;
					sB = ((factor * (sB - 128)) >> 8) + 128;

					sR = sR < 0 ? 0 :
						sR > 255 ? 255 : sR;
					sG = sG < 0 ? 0 :
						sG > 255 ? 255 : sG;
					sB = sB < 0 ? 0 :
						sB > 255 ? 255 : sB;

					pixels[i] = sA << 24 | sR << 16 | sG << 8 | sB;
				}
			}
		}
	}
}
