﻿using System;
using System.Runtime.InteropServices;
using System.Windows;

namespace xDraw.StandardFilters
{
    internal class WeightedMeanSquareMapFilter : BaseMapFilter
    {
        private readonly int[] _weightMap;

        public WeightedMeanSquareMapFilter(int[] weightMap)
        {
            _weightMap = weightMap;
            AssertMap();
        }

        private void AssertMap()
        {
            double sqrt = Math.Sqrt(_weightMap.Length);
            // ReSharper disable once CompareOfFloatsByEqualityOperator
            if (sqrt != (int)sqrt)
            {
                throw new ArgumentException("Map provided should have square shape");
            }

            if ((int)sqrt % 2 == 0)
            {
                throw new ArgumentException("Width and height of map should be odd.");
            }
        }

        public override unsafe void ApplyMap(int* pixels, Size size, bool ignoreAlpha = false)
        {
            var mapWidth = (int)Math.Sqrt(_weightMap.Length);
            int mapDistance = mapWidth / 2;
            var pxWidth = (int)size.Width;
            var pxHeight = (int)size.Height;
            var tmpBuf = new int[pxWidth * pxHeight];

            for (var x = 0; x < pxWidth; x++)
            {
                OnProgress(x * 100.0 / pxWidth);
                for (var y = 0; y < pxHeight; y++)
                {
                    var aMean = 0;
                    var rMean = 0;
                    var gMean = 0;
                    var bMean = 0;
                    var meanCounter = 0;
                    for (int mx = -mapDistance; mx <= mapDistance; mx++)
                    {
                        for (int my = -mapDistance; my <= mapDistance; my++)
                        {
                            if (x + mx < 0 || x + mx >= pxWidth)
                                continue;
                            if (y + my < 0 || y + my >= pxHeight)
                                continue;
                            int weight = _weightMap[mapWidth * (my + mapDistance) + mx + mapDistance];
                            int pixel = pixels[(y + my) * pxWidth + x + mx];
                            int sA = pixel >> 24 & byte.MaxValue;
                            int sR = pixel >> 16 & byte.MaxValue;
                            int sG = pixel >> 8 & byte.MaxValue;
                            int sB = pixel & byte.MaxValue;
                            if (ignoreAlpha && mx == 0 && my == 0)
                                aMean = sA;
                            else if (!ignoreAlpha)
                                aMean += weight * sA;
                            rMean += weight * sR;
                            gMean += weight * sG;
                            bMean += weight * sB;
                            meanCounter += weight;
                        }
                    }

                    if (meanCounter == 0)
                    {
                        meanCounter = 1;
                    }

                    int dA = ignoreAlpha ? aMean : aMean / meanCounter;
                    dA = dA < 0 ? 0 :
                        dA > 255 ? 255 : dA;
                    int dR = rMean / meanCounter;
                    dR = dR < 0 ? 0 :
                        dR > 255 ? 255 : dR;
                    dR = ((dR + 1) * dA) >> 8;
                    int dG = gMean / meanCounter;
                    dG = dG < 0 ? 0 :
                        dG > 255 ? 255 : dG;
                    dG = ((dG + 1) * dA) >> 8;
                    int dB = bMean / meanCounter;
                    dB = dB < 0 ? 0 :
                        dB > 255 ? 255 : dB;
                    dB = ((dB + 1) * dA) >> 8;

                    tmpBuf[y * pxWidth + x] = dA << 24 | dR << 16 | dG << 8 | dB;
                }
            }

            Marshal.Copy(tmpBuf, 0, new IntPtr(pixels), tmpBuf.Length);
        }
    }
}
