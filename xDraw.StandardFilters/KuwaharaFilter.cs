﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Windows;

namespace xDraw.StandardFilters
{
    internal class KuwaharaFilter : BaseMapFilter
    {
        private readonly int _distance;

        public KuwaharaFilter(int distance)
        {
            _distance = distance;
        }

        public override unsafe void ApplyMap(int* pixels, Size size, bool ignoreAlpha = false)
        {
            var pxWidth = (int)size.Width;
            var pxHeight = (int)size.Height;
            var tmpBuf = new int[pxWidth * pxHeight];
            int regionWidth = _distance + 1;
            var regionA = new int[regionWidth * regionWidth];
            var regionR = new int[regionWidth * regionWidth];
            var regionG = new int[regionWidth * regionWidth];
            var regionB = new int[regionWidth * regionWidth];
            var regionAMean = new int[4];
            var regionRMean = new int[4];
            var regionGMean = new int[4];
            var regionBMean = new int[4];
            var regionAVariance = new int[4];
            var regionRVariance = new int[4];
            var regionGVariance = new int[4];
            var regionBVariance = new int[4];
            for (var x = 0; x < pxWidth; x++)
            {
                OnProgress(x * 100.0 / pxWidth);
                for (var y = 0; y < pxHeight; y++)
                {

                    for (var rX = 0; rX <= 1; rX++)
                    {
                        for (var rY = 0; rY <= 1; rY++)
                        {
                            for (var mx = 0; mx < regionWidth; mx++)
                            {
                                for (var my = 0; my < regionWidth; my++)
                                {
                                    int xOffset = mx - _distance + rX * regionWidth;
                                    int yOffset = my - _distance + rY * regionWidth;
                                    if (x + xOffset < 0 || x + xOffset >= pxWidth ||
                                        y + yOffset < 0 || y + yOffset >= pxHeight)
                                    {
                                        regionA[my * regionWidth + mx] = -1;
                                        regionR[my * regionWidth + mx] = -1;
                                        regionG[my * regionWidth + mx] = -1;
                                        regionB[my * regionWidth + mx] = -1;
                                        continue;
                                    }

                                    int pixel = pixels[(y + yOffset) * pxWidth + x + xOffset];
                                    int sA = pixel >> 24 & byte.MaxValue;
                                    int sR = pixel >> 16 & byte.MaxValue;
                                    int sG = pixel >> 8 & byte.MaxValue;
                                    int sB = pixel & byte.MaxValue;

                                    regionA[my * regionWidth + mx] = sA;
                                    regionR[my * regionWidth + mx] = sR;
                                    regionG[my * regionWidth + mx] = sG;
                                    regionB[my * regionWidth + mx] = sB;
                                }
                            }

                            regionAMean[rY * 2 + rX] = CountMean(regionA.SkipWhile(e => e < 0));
                            regionRMean[rY * 2 + rX] = CountMean(regionR.SkipWhile(e => e < 0));
                            regionGMean[rY * 2 + rX] = CountMean(regionG.SkipWhile(e => e < 0));
                            regionBMean[rY * 2 + rX] = CountMean(regionB.SkipWhile(e => e < 0));
                            regionAVariance[rY * 2 + rX] = CountVariance(regionA.SkipWhile(e => e < 0), regionAMean[rY * 2 + rX]);
                            regionRVariance[rY * 2 + rX] = CountVariance(regionR.SkipWhile(e => e < 0), regionRMean[rY * 2 + rX]);
                            regionGVariance[rY * 2 + rX] = CountVariance(regionG.SkipWhile(e => e < 0), regionGMean[rY * 2 + rX]);
                            regionBVariance[rY * 2 + rX] = CountVariance(regionB.SkipWhile(e => e < 0), regionBMean[rY * 2 + rX]);
                        }
                    }

                    int minVarianceRegion = Array.IndexOf(regionAVariance, regionAVariance.Min());
                    int dA = regionAMean[minVarianceRegion];
                    minVarianceRegion = Array.IndexOf(regionRVariance, regionRVariance.Min());
                    int dR = regionRMean[minVarianceRegion];
                    minVarianceRegion = Array.IndexOf(regionGVariance, regionGVariance.Min());
                    int dG = regionGMean[minVarianceRegion];
                    minVarianceRegion = Array.IndexOf(regionBVariance, regionBVariance.Min());
                    int dB = regionBMean[minVarianceRegion];
                    tmpBuf[y * pxWidth + x] = dA << 24 | dR << 16 | dG << 8 | dB;
                }
            }
            Marshal.Copy(tmpBuf, 0, new IntPtr(pixels), tmpBuf.Length);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static int CountVariance(IEnumerable<int> elements, int mean)
        {
            IEnumerable<int> enumerable = elements as int[] ?? elements.ToArray();
            if (!enumerable.Any())
            {
                return 0;
            }

            return enumerable.Sum(e => (mean - e) * (mean - e)) / enumerable.Count();
        }


        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static int CountMean(IEnumerable<int> regionA)
        {
            IEnumerable<int> enumerable = regionA as int[] ?? regionA.ToArray();
            if (!enumerable.Any())
            {
                return 0;
            }

            return enumerable.Sum() / enumerable.Count();
        }
    }
}
