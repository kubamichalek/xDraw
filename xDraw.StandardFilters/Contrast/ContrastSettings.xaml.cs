﻿using System.Windows;

namespace xDraw.StandardFilters.Contrast
{

    public partial class ConstrastSettings
    {
        public ConstrastSettings()
        {
            DataContext = this;
            InitializeComponent();
        }

        public int Delta { get; set; }

        private void OkClicked(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }
    }
}
