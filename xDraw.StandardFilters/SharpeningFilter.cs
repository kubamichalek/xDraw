﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Media;
using xDraw.Common.Modals;
using xDraw.Logic.Filters;

namespace xDraw.StandardFilters
{
	internal class SharpeningFilter : BaseFilter
	{
		public SharpeningFilter(IFilterManager filterManager) : base(filterManager)
		{
		}

		public override string Name => "Wyostrzanie";

		public override unsafe void Execute(int* pixels, Size size, PixelFormat format)
		{
			var weightMap = new[]
			{
				0, -1, 0,
				-1, 5, 1,
				0, -1, 0
			};
			var filter = new WeightedMeanSquareMapFilter(weightMap);
			var worker = new BackgroundWorker();
			var progressBar = new ProgressModal
			{
				Title = Name,
				Description = "Nakładanie filtra w trakcie..."
			};
			filter.Progress.Subscribe(v => progressBar.ProgressValue = v);
			worker.DoWork += (sender, args) =>
			{
				filter.ApplyMap(pixels, size);
			};
			worker.RunWorkerCompleted += (sender, args) => progressBar.Close();
			worker.RunWorkerAsync();
			progressBar.ShowDialog();
		}
	}
}
