﻿using System.Windows;
using System.Windows.Media;
using xDraw.Logic.Filters;
using xDraw.StandardFilters.Brightness;

namespace xDraw.StandardFilters
{
	internal class BrightnessFilter : BaseFilter
	{
		public BrightnessFilter(IFilterManager filterManager) : base(filterManager)
		{
		}

		public override string Name => "Jaskrawość";

		public override unsafe void Execute(int* pixels, Size size, PixelFormat format)
		{
			var settings = new BrightnessSettings();
			if (settings.ShowDialog().GetValueOrDefault())
			{
				var delta = (int)(settings.Delta / 100f * 255);
				var length = (int)(size.Width * size.Height);
				for (var i = 0; i < length; i++)
				{
					int c = pixels[i];
					int sA = c >> 24 & byte.MaxValue;
					int sR = c >> 16 & byte.MaxValue;
					int sG = c >> 8 & byte.MaxValue;
					int sB = c & byte.MaxValue;

					sR += delta;
					sG += delta;
					sB += delta;

					sR = sR < 0 ? 0 :
						sR > 255 ? 255 : sR;
					sG = sG < 0 ? 0 :
						sG > 255 ? 255 : sG;
					sB = sB < 0 ? 0 :
						sB > 255 ? 255 : sB;

					pixels[i] = sA << 24 | sR << 16 | sG << 8 | sB;
				}
			}
		}
	}
}
