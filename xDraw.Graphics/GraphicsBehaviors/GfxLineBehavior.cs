﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace xDraw.GFX.GraphicsBehaviors
{
    public class GfxLineBehavior : IGfxBehavior
    {
        private readonly WriteableBitmap _bitmap;
        private readonly Point _p1;
        private readonly Point _p2;
        private readonly int _thickness;
        private readonly Color _color;

        public GfxLineBehavior(WriteableBitmap bitmap, Point p1, Point p2, int thickness, Color color)
        {
            _bitmap = bitmap;
            _p1 = p1;
            _p2 = p2;
            _thickness = thickness;
            _color = color;
        }

        public void Execute()
        {
            var x1 = (int)_p1.X;
            var y1 = (int)_p1.Y;
            var x2 = (int)_p2.X;
            var y2 = (int)_p2.Y;
            if (_thickness == 1)
            {
                _bitmap.DrawLineDDA(x1, y1, x2, y2, _color);
                return;
            }

            double length = Math.Sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
            int linesAmount = _thickness / 2;

            var ux1 = (int)(x1 + linesAmount * (y2 - y1) / length);
            var uy1 = (int)(y1 + linesAmount * (x1 - x2) / length);
            var ux2 = (int)(x2 + linesAmount * (y2 - y1) / length);
            var uy2 = (int)(y2 + linesAmount * (x1 - x2) / length);
            var dx1 = (int)(x1 - linesAmount * (y2 - y1) / length);
            var dy1 = (int)(y1 - linesAmount * (x1 - x2) / length);
            var dx2 = (int)(x2 - linesAmount * (y2 - y1) / length);
            var dy2 = (int)(y2 - linesAmount * (x1 - x2) / length);

            _bitmap.FillPolygon(new[] { ux1, uy1, ux2, uy2, dx2, dy2, dx1, dy1, ux1, uy1 }, _color);
        }
    }
}