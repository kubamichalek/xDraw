﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace xDraw.GFX.GraphicsBehaviors
{
	internal class GfxFloodFillBehavior : IGfxBehavior
	{
		private readonly WriteableBitmap _bitmap;
		private readonly Point _point;
		private readonly int _tolerance;
		private readonly Color _color;

		public GfxFloodFillBehavior(WriteableBitmap bitmap, Color color, Point point, int tolerance)
		{
			_bitmap = bitmap;
			_point = point;
			_tolerance = tolerance;
			_color = color;
		}

		public void Execute()
		{
			using (BitmapContext context = _bitmap.GetBitmapContext())
			{
				FloodFill(context, (int)_point.X, (int)_point.Y, _bitmap.PixelWidth,
					_bitmap.PixelHeight, WriteableBitmapExtensions.ConvertColor(_color),
					_bitmap.GetPixeli((int)_point.X, (int)_point.Y));
			}
		}

		private unsafe void FloodFill(BitmapContext bitmap, int x, int y, int width, int height, int repCol, int tarCol)
		{
			if (bitmap.Pixels[GetIndex(x, y, width)] != tarCol ||
				bitmap.Pixels[GetIndex(x, y, width)] == repCol)
			{
				return;
			}

			var queue = new Queue<Tuple<int, int>>();
			queue.Enqueue(new Tuple<int, int>(x, y));

			while (queue.Any())
			{
				Tuple<int, int> point = queue.Dequeue();
				int tc = bitmap.Pixels[GetIndex(point.Item1, point.Item2, width)];
				if (!ColorEqual(tc, tarCol) || ColorEqual(tc, repCol))
				{
					continue;
				}

				for (int dx = point.Item1; dx >= 0; dx--)
				{
					int bc = bitmap.Pixels[GetIndex(dx, point.Item2, width)];
					if (!ColorEqual(bc, tarCol) || ColorEqual(bc, repCol))
					{
						break;
					}
					bitmap.Pixels[GetIndex(dx, point.Item2, width)] = repCol;
					if (point.Item2 + 1 < height && !ColorEqual(bitmap.Pixels[GetIndex(dx, point.Item2 + 1, width)], repCol))
					{
						queue.Enqueue(new Tuple<int, int>(dx, point.Item2 + 1));
					}
					if (point.Item2 - 1 >= 0 && !ColorEqual(bitmap.Pixels[GetIndex(dx, point.Item2 - 1, width)], repCol))
					{
						queue.Enqueue(new Tuple<int, int>(dx, point.Item2 - 1));
					}
				}
				for (int dx = point.Item1 + 1; dx < width; dx++)
				{
					int bc = bitmap.Pixels[GetIndex(dx, point.Item2, width)];
					if (!ColorEqual(bc, tarCol) || ColorEqual(bc, repCol))
					{
						break;
					}
					bitmap.Pixels[GetIndex(dx, point.Item2, width)] = repCol;
					if (point.Item2 + 1 < height && !ColorEqual(bitmap.Pixels[GetIndex(dx, point.Item2 + 1, width)], repCol))
					{
						queue.Enqueue(new Tuple<int, int>(dx, point.Item2 + 1));
					}
					if (point.Item2 - 1 >= 0 && !ColorEqual(bitmap.Pixels[GetIndex(dx, point.Item2 - 1, width)], repCol))
					{
						queue.Enqueue(new Tuple<int, int>(dx, point.Item2 - 1));
					}
				}
			}
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		private bool ColorEqual(int color1, int color2)
		{
			int c1A = color1 >> 24 & byte.MaxValue;
			int c1R = color1 >> 16 & byte.MaxValue;
			int c1G = color1 >> 8 & byte.MaxValue;
			int c1B = color1 & byte.MaxValue;
			int c2A = color2 >> 24 & byte.MaxValue;
			int c2R = color2 >> 16 & byte.MaxValue;
			int c2G = color2 >> 8 & byte.MaxValue;
			int c2B = color2 & byte.MaxValue;
			int distance = Math.Abs(c1A - c2A) * Math.Abs(c1A - c2A) + Math.Abs(c1R - c2R) * Math.Abs(c1R - c2R) +
						   Math.Abs(c1G - c2G) * Math.Abs(c1G - c2G) + Math.Abs(c1B - c2B) * Math.Abs(c1B - c2B);
			return distance <= _tolerance;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		private static int GetIndex(int x, int y, int width)
		{
			return y * width + x;
		}
	}
}