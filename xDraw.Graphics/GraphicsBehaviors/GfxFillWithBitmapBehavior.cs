﻿using System.Windows.Media;
using System.Windows.Media.Imaging;
using xDraw.GFX.Utils;

namespace xDraw.GFX.GraphicsBehaviors
{
    public class GfxFillWithBitmapBehavior : IGfxBehavior
    {
        private readonly WriteableBitmap _targetBitmap;
        private readonly WriteableBitmap _sourceBitmap;
        private Color? _replacementColor;

        public GfxFillWithBitmapBehavior(WriteableBitmap targetBitmap, WriteableBitmap sourceBitmap)
        {
            _targetBitmap = targetBitmap;
            _sourceBitmap = sourceBitmap;
        }

        public GfxFillWithBitmapBehavior(WriteableBitmap targetBitmap, WriteableBitmap sourceBitmap, Color replacementColor) : this(targetBitmap, sourceBitmap)
        {
            _replacementColor = replacementColor;
        }

        public unsafe void Execute()
        {
            using (BitmapContext ctx = _targetBitmap.GetBitmapContext())
            using (BitmapContext sCtx = _sourceBitmap.GetBitmapContext())
            {
                for (var x = 0; x < ctx.Width * ctx.Height; x++)
                {
                    int srcPixel = sCtx.Pixels[x];
                    int dstPixel = ctx.Pixels[x];
                    uint sA = (uint)srcPixel >> 24 & byte.MaxValue;
                    if (sA == 0)
                        continue;
                    uint sR;
                    uint sG;
                    uint sB;
                    if (_replacementColor != null)
                    {
                        Color c = _replacementColor.Value;
                        c.A = (byte)sA;
                        c = c.PremultiplyAlpha();
                        sR = c.R;
                        sG = c.G;
                        sB = c.B;
                    }
                    else
                    {
                        sR = (uint)srcPixel >> 16 & byte.MaxValue;
                        sG = (uint)srcPixel >> 8 & byte.MaxValue;
                        sB = (uint)srcPixel & byte.MaxValue;
                    }
                    uint dA = (uint)dstPixel >> 24 & byte.MaxValue;
                    uint dR = (uint)dstPixel >> 16 & byte.MaxValue;
                    uint dG = (uint)dstPixel >> 8 & byte.MaxValue;
                    uint dB = (uint)dstPixel & byte.MaxValue;


                    dA = sA + dA * (255 - sA) / 255;
                    dA <<= 24;
                    dR = sR + (dR * (255 - sA) >> 8);
                    dR <<= 16;
                    dG = sG + (dG * (255 - sA) >> 8);
                    dG <<= 8;
                    dB = sB + (dB * (255 - sA) >> 8);

                    ctx.Pixels[x] = (int)(dA | dR | dG | dB);
                }
            }
        }
    }
}