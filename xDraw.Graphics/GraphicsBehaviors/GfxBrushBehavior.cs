﻿using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace xDraw.GFX.GraphicsBehaviors
{
	public class GfxBrushBehavior : IGfxBehavior
	{
		private readonly WriteableBitmap _bitmap;
		private readonly Point _p1;
		private readonly Point _p2;
		private readonly Color _color;
		private readonly WriteableBitmap _brushImage;


		public GfxBrushBehavior(WriteableBitmap bitmap, Color color, Point p1, Point p2, WriteableBitmap brushImage)
		{
			_bitmap = bitmap;
			_color = color;
			_p1 = (Point)(p1 - new Point(brushImage.Width / 2, brushImage.Height / 2));
			_p2 = (Point)(p2 - new Point(brushImage.Width / 2, brushImage.Height / 2));
			_brushImage = brushImage;
		}

		public void Execute()
		{
			var transform = new TranslateTransform(_p1.X, _p1.Y);
			var tmp = new WriteableBitmap(_bitmap);
			tmp.Clear();
			tmp.BlitRender(_brushImage, false, 1, transform);
			new GfxFillWithBitmapBehavior(_bitmap, tmp, _color).Execute();
		}
	}
}