﻿using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace xDraw.GFX.GraphicsBehaviors
{
    public class GfxRectangleBehavior : IGfxBehavior
    {
        private readonly WriteableBitmap _bitmap;
        private readonly int _thickness;
        private readonly Color _color;
        private Point _p1;
        private Point _p2;

        public GfxRectangleBehavior(WriteableBitmap bitmap, Point p1, Point p2, int thickness, Color color)
        {
            _bitmap = bitmap;
            _p1 = p1;
            _p2 = p2;
            _thickness = thickness;
            _color = color;

            ShufflePointsIfNeeded();
        }

        private void ShufflePointsIfNeeded()
        {
            if (_p1.X >= _p2.X && _p1.Y >= _p2.Y)
            {
                Point temp = _p2;
                _p2 = _p1;
                _p1 = temp;
            }
            else if (_p1.X < _p2.X && _p1.Y >= _p2.Y)
            {
                double tmp = _p2.Y;
                _p2.Y = _p1.Y;
                _p1.Y = tmp;
            }
            else if (_p1.X >= _p2.X && _p1.Y < _p2.Y)
            {
                double tmp = _p2.X;
                _p2.X = _p1.X;
                _p1.X = tmp;
            }
        }

        public void Execute()
        {
            var x1 = (int)_p1.X;
            var y1 = (int)_p1.Y;
            var x2 = (int)_p2.X;
            var y2 = (int)_p2.Y;


            _bitmap.DrawRectangle(x1, y1, x2, y2, _color);

            for (var i = 0; i < _thickness; i++)
            {
                _bitmap.DrawRectangle(x1--, y1--, x2++, y2++, _color);
            }
        }
    }
}