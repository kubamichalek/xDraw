﻿using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace xDraw.GFX.GraphicsBehaviors
{
    internal class GfxFillBehavior : IGfxBehavior
    {
        private readonly WriteableBitmap _bitmap;
        private readonly Color _color;

        public GfxFillBehavior(WriteableBitmap bitmap, Color color)
        {
            _bitmap = bitmap;
            _color = color;
        }

        public void Execute()
        {
            if (_color == Colors.Transparent)
            {
                _bitmap.Clear();
            }
            else
            {
                _bitmap.Clear(_color);
            }
        }
    }
}