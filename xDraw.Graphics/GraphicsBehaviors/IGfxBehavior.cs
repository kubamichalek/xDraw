﻿namespace xDraw.GFX.GraphicsBehaviors
{
    internal interface IGfxBehavior
    {
        void Execute();
    }
}