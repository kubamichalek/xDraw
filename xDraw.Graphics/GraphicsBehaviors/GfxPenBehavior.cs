﻿using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using xDraw.GFX.Utils;

namespace xDraw.GFX.GraphicsBehaviors
{
    public class GfxPenBehavior : IGfxBehavior
    {
        private readonly WriteableBitmap _bitmap;
        private readonly Point _p1;
        private readonly Point _p2;
        private readonly Color _color;

        public GfxPenBehavior(WriteableBitmap bitmap, Color color, Point p1, Point p2)
        {
            _bitmap = bitmap;
            _p1 = p1;
            _p2 = p2;
            _color = color;
        }

        public void Execute()
        {
            _bitmap.DrawLine((int)_p1.X, (int)_p1.Y, (int)_p2.X, (int)_p2.Y, _color);
        }
    }
}