﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using xDraw.GFX.GraphicsBehaviors;

namespace xDraw.GFX
{
	/// <inheritdoc />
	/// <summary>
	///     Assumes that bitmap is in Pbgra32 format.
	/// </summary>
	public class Graphics : IDisposable
	{
		private readonly WriteableBitmap _bitmap;

		public Graphics(WriteableBitmap bitmap)
		{
			_bitmap = bitmap;
		}

		public void Dispose()
		{

		}

		public void Fill(Color color)
		{
			ExecuteBehavior(new GfxFillBehavior(_bitmap, color));
		}

		public void DrawPen(Color color, Point p1, Point p2)
		{
			ExecuteBehavior(new GfxPenBehavior(_bitmap, color, p1, p2));
		}

		public void DrawBrush(Color color, Point p1, Point p2, WriteableBitmap brushImage)
		{
			ExecuteBehavior(new GfxBrushBehavior(_bitmap, color, p1, p2, brushImage));
		}

		public void FillWithBitmap(WriteableBitmap bitmap)
		{
			ExecuteBehavior(new GfxFillWithBitmapBehavior(_bitmap, bitmap));
		}

		public void Clear()
		{
			Fill(Colors.Transparent);
		}

		public void DrawLine(Point p1, Point p2, int thickness, Color color)
		{
			ExecuteBehavior(new GfxLineBehavior(_bitmap, p1, p2, thickness, color));
		}

		public void DrawRectangle(Point p1, Point p2, int thickness, Color color)
		{
			ExecuteBehavior(new GfxRectangleBehavior(_bitmap, p1, p2, thickness, color));
		}

		public void FloodFill(Color color, Point point, int tolerance)
		{
			ExecuteBehavior(new GfxFloodFillBehavior(_bitmap, color, point, tolerance));
		}

		public static WriteableBitmap MergeBitmaps(IEnumerable<WriteableBitmap> bitmaps)
		{
			IEnumerable<WriteableBitmap> writeableBitmaps = bitmaps as WriteableBitmap[] ?? bitmaps.ToArray();
			if (!writeableBitmaps.Any())
			{
				throw new ArgumentException("No bitmaps provided", nameof(bitmaps));
			}

			WriteableBitmap bottom = writeableBitmaps.Last();
			WriteableBitmap fresh = bottom.Clone();
			using (var gfx = new Graphics(fresh))
			{
				foreach (WriteableBitmap bitmap in writeableBitmaps.Reverse().Skip(1).Select(l => l))
				{
					gfx.FillWithBitmap(bitmap);
				}
			}

			return fresh;
		}

		private static void ExecuteBehavior(IGfxBehavior behavior)
		{
			behavior.Execute();
		}
	}
}