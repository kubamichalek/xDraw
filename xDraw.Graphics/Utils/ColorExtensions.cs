﻿using System.Windows.Media;

namespace xDraw.GFX.Utils
{
	public static class ColorExtensions
	{
		public static Color PremultiplyAlpha(this Color color)
		{
			float alphaBlend = color.A / 255.0f;
			color.B = (byte) (color.B * alphaBlend);
			color.G = (byte) (color.G * alphaBlend);
			color.R = (byte) (color.R * alphaBlend);
			return color;
		}
	}
}