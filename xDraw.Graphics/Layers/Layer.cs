using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using GalaSoft.MvvmLight.CommandWpf;
using Mathtone.MIST;
using xDraw.Common;

namespace xDraw.GFX.Layers
{
    [Notifier]
    public class Layer : NotifierBase
    {
        public Layer(int width, int height, Color fillColor, string initialName)
        {
            Bitmap = BitmapFactory.New(width, height);

            using (var g = new Graphics(Bitmap))
            {
                g.Fill(fillColor);
            }
            Drawing = new ImageDrawing(Bitmap, new Rect(0, 0, Bitmap.Width, Bitmap.Height));
            Name = initialName;

            InitializeCommands();
        }

        [Notify]
        public Drawing Drawing { get; }

        public WriteableBitmap Bitmap { get; private set; }

        [Notify]
        public string Name { get; set; }

        [Notify]
        public bool EditNameMode { get; set; }

        public ICommand TurnOnEditModeCommand { get; private set; }

        public ICommand SaveNewNameCommand { get; private set; }

        private void InitializeCommands()
        {
            TurnOnEditModeCommand = new RelayCommand<RoutedEventArgs>(OnTurnOnEditMode);
            SaveNewNameCommand = new RelayCommand(OnSaveNewName);
        }

        private void OnSaveNewName()
        {
            EditNameMode = false;
        }

        private void OnTurnOnEditMode(RoutedEventArgs e)
        {
            EditNameMode = true;
            e.Handled = true;
        }

        public void MergeWith(Layer layerToMerge)
        {
            using (var g = new Graphics(Bitmap))
            {
                g.FillWithBitmap(layerToMerge.Bitmap);
            }
        }
    }
}