﻿using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using xDraw.Logic.Projects.Models;

namespace xDraw.UI.ViewModels.ViewModels
{
	public class NewProjectModalViewModel : ViewModelBase
	{
		public NewProjectModalViewModel(NewProjectModel model)
		{
			Model = model;
			Model.ProjectName = "Nowy projekt";
			Model.CanvasWidth = 640;
			Model.CanvasHeight = 480;
			Model.CanvasBackgroundColor = Colors.Transparent;

			InitializeCommands();
		}

		public NewProjectModel Model { get; }

		public ICommand OkCommand { get; private set; }

		public ICommand CancelCommand { get; private set; }

		private void InitializeCommands()
		{
			OkCommand = new RelayCommand<Window>(OnOkCommand);
			CancelCommand = new RelayCommand<Window>(OnCancelCommand);
		}

		private static void OnOkCommand(Window window)
		{
			window.DialogResult = true;
			window.Close();
		}

		private static void OnCancelCommand(Window window)
		{
			window.DialogResult = false;
			window.Close();
		}
	}
}