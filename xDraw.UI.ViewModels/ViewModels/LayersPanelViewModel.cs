﻿using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using xDraw.GFX.Layers;
using xDraw.Logic.Projects;

namespace xDraw.UI.ViewModels.ViewModels
{
    public class LayersPanelViewModel : ViewModelBase, IDisposable
    {
        private readonly IProjectManager _projectManager;
        private readonly IDisposable _projectManagerObserver;


        public LayersPanelViewModel(IProjectManager projectManager)
        {
            _projectManager = projectManager;
            _projectManagerObserver = _projectManager.Loaded.Subscribe(OnProjectLoadedChanged);

            InitializeCommands();
        }

        public ICommand CreateLayerCommand { get; private set; }
        public ICommand DeleteLayerCommand { get; private set; }
        public ICommand MergeAllCommand { get; private set; }
        public ICommand FinishLayerRearrange { get; private set; }

        public ObservableCollection<Layer> Layers => _projectManager.Project?.Layers;

        public Layer ActiveLayer
        {
            get => _projectManager.Project?.ActiveLayer;
            set
            {
                if (value == null)
                    return;

                _projectManager.Project.ActiveLayer = value;
            }
        }

        public void Dispose()
        {
            _projectManagerObserver?.Dispose();
        }

        private void InitializeCommands()
        {
            CreateLayerCommand = new RelayCommand(OnCreateLayer);
            MergeAllCommand = new RelayCommand(OnMergeAll, () => Layers?.Count > 1);
            DeleteLayerCommand = new RelayCommand(OnDeleteLayer, () => Layers?.Count > 1);
            FinishLayerRearrange = new RelayCommand<DragEventArgs>(FinishDragging);
        }

        private void FinishDragging(DragEventArgs e)
        {
            var droppedData = e.Data.GetData(typeof(Layer)) as Layer;
            var targetLbi = (ListBoxItem)((FrameworkElement)e.Source).TemplatedParent;
            var target = (Layer)targetLbi.Content;
            int removedIdx = Layers.IndexOf(droppedData);
            int targetIdx = Layers.IndexOf(target);

            if (removedIdx < targetIdx)
            {
                Layers.Move(removedIdx, targetIdx);
            }
            else
            {
                if (Layers.Count > removedIdx)
                {
                    Layers.Move(removedIdx, targetIdx);
                }
            }
        }

        private void OnMergeAll()
        {
            if (!UserConfirms("Jeste pewien, że chcesz scalić wszystkie warstwy w jedną")) return;
            _projectManager.Project.MergeLayers();
            RaisePropertyChanged(() => ActiveLayer);
        }

        private void OnDeleteLayer()
        {
            if (!UserConfirms($"Jesteś pewien, że chcesz usunąć warstwę \"{ActiveLayer.Name}\"")) return;
            _projectManager.Project.DeleteActiveLayer();
            RaisePropertyChanged(() => ActiveLayer);
        }

        private bool UserConfirms(string question)
        {
            MessageBoxResult result = MessageBox.Show(
                question,
                "Pytanie",
                MessageBoxButton.YesNo,
                MessageBoxImage.Question,
                MessageBoxResult.No);
            return result == MessageBoxResult.Yes;
        }

        private void OnCreateLayer()
        {
            _projectManager.Project.CreateNewLayer();
        }

        private void OnProjectLoadedChanged(bool loaded)
        {
            RaisePropertyChanged(() => Layers);
            RaisePropertyChanged(() => ActiveLayer);
        }
    }
}