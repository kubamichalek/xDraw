using System;
using System.Collections.Generic;
using System.Reactive;
using System.Windows.Controls;
using System.Windows.Media;
using GalaSoft.MvvmLight;
using xDraw.Logic.Projects;
using xDraw.Logic.Tools;

namespace xDraw.UI.ViewModels.ViewModels
{
    public class MainViewModel : ViewModelBase, IDisposable
    {
        private readonly IProjectManager _projectManager;
        private readonly IToolsManager _toolsManager;
        private readonly IDisposable _projectLoadedObserver;
        private readonly IDisposable _activeToolObserver;
        private string _windowTitle;

        public MainViewModel(IProjectManager projectManager, IToolsManager toolsManager)
        {
            _projectManager = projectManager;
            _toolsManager = toolsManager;
            _projectLoadedObserver = projectManager.Loaded.Subscribe(OnProjectLoadedChanged);
            _activeToolObserver = toolsManager.ToolChanged.Subscribe(OnToolChanged);
        }

        public string WindowTitle
        {
            get => _windowTitle;
            set => Set(ref _windowTitle, value);
        }

        public bool IsProjectLoaded => _projectManager.IsLoaded;

        public ICollection<ITool> Tools => _toolsManager.AvailableTools;

        public Color SelectedColor
        {
            get => _projectManager.Project?.SelectedColor ?? Colors.Black;
            set => _projectManager.Project.SelectedColor = value;
        }

        public Control CurrentToolSettings => _toolsManager.CurrentTool?.SettingsPanelControl;

        public void Dispose()
        {
            _projectLoadedObserver.Dispose();
            _activeToolObserver.Dispose();
        }

        private void OnProjectLoadedChanged(bool loaded)
        {
            if (!loaded) return;
            WindowTitle = _projectManager.Project.Name;
            RaisePropertyChanged(string.Empty);
        }

        private void OnToolChanged(EventPattern<object> e)
        {
            RaisePropertyChanged(nameof(CurrentToolSettings));
        }
    }
}