﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using Microsoft.Win32;
using xDraw.Logic.Filters;
using xDraw.Logic.Projects;

namespace xDraw.UI.ViewModels.ViewModels
{
    public class MainMenuItemsViewModel : ViewModelBase
    {
        private readonly IProjectManager _projectManager;
        private readonly IFilterManager _filterManager;

        public MainMenuItemsViewModel(IProjectManager projectManager, IFilterManager filterManager)
        {
            _projectManager = projectManager;
            _filterManager = filterManager;
            _projectManager.Loaded.Subscribe(OnProjectLoadedChanged);
            Filters = filterManager.Filters;
            InitializeCommands();
        }

        #region File
        public ICommand NewProjectCommand { get; private set; }
        public ICommand OpenFileCommand { get; private set; }
        public RelayCommand SaveCommand { get; private set; }
        public RelayCommand SaveAsCommand { get; private set; }
        public ICommand ExitCommand { get; private set; }
        #endregion

        #region Filters
        public ICollection<IFilter> Filters { get; }
        public ICommand FilterInvokeCommand { get; private set; }
        #endregion


        private void InitializeCommands()
        {
            NewProjectCommand = new RelayCommand(OnNewProject);
            OpenFileCommand = new RelayCommand(OnOpenFile);
            SaveCommand = new RelayCommand(OnSave, () => _projectManager.IsLoaded);
            SaveAsCommand = new RelayCommand(OnSaveAs, () => _projectManager.IsLoaded);
            ExitCommand = new RelayCommand(OnExit);

            FilterInvokeCommand = new RelayCommand<IFilter>(ExecuteFilter);
        }

        private void ExecuteFilter(IFilter filter)
        {
            _filterManager.Start(filter, _projectManager.Project.ActiveLayer.Bitmap);
        }

        private void OnSaveAs()
        {
            var saveDialog = new SaveFileDialog
            {
                Title = "Zapisz projekt jako",
                OverwritePrompt = true,
                AddExtension = true,
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
                Filter = "Obraz JPEG|*.jpg; *.jpeg" +
                         "|Obraz PNG|*.png"
            };
            if (!saveDialog.ShowDialog().GetValueOrDefault())
            {
                return;
            }

            string fileName = saveDialog.FileName;
            _projectManager.SaveCurrentProjectToFile(fileName);
        }

        private void OnSave()
        {
            if (!_projectManager.Project.IsFileBased)
            {
                OnSaveAs();
            }

            _projectManager.Project.Save();
        }

        private void OnOpenFile()
        {
            var openDialog = new OpenFileDialog
            {
                Title = "Otwórz projekt lub plik",
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
                Multiselect = false,
                Filter = "Image files|*.jpg; *.jpeg; *.png; *.bmp"
            };
            if (!openDialog.ShowDialog().GetValueOrDefault())
            {
                return;
            }

            string fileName = openDialog.FileName;
            try
            {
                _projectManager.LoadProjectFromFile(fileName);
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Błąd otwierania pliku", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private static void OnExit()
        {
            Application.Current.MainWindow?.Close();
        }

        private void OnNewProject()
        {
            _projectManager.CreateNewProject();
        }

        private void OnProjectLoadedChanged(bool loaded)
        {
            SaveCommand.RaiseCanExecuteChanged();
            SaveAsCommand.RaiseCanExecuteChanged();
        }
    }
}