﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Reactive;
using System.Reactive.Linq;
using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight.CommandWpf;
using Mathtone.MIST;
using xDraw.Common;
using xDraw.Common.EventArgs;
using xDraw.GFX.Layers;
using xDraw.Logic.GfxCanvas;
using xDraw.Logic.Projects;

namespace xDraw.UI.ViewModels.ViewModels
{
    [Notifier]
    public class XDrawCanvasViewModel : ViewModelNotifierBase, IDisposable
    {
        private readonly IProjectManager _projectManager;
        private readonly ICanvasNotifier _canvas;
        private IDisposable _layersChangedObserver;

        public XDrawCanvasViewModel(IProjectManager projectManager, ICanvasNotifier canvas)
        {
            _projectManager = projectManager;
            _canvas = canvas;
            _projectManager.Loaded.Subscribe(OnProjectLoadedChanged);

            InitializeCommands();
        }

        public IEnumerable<Layer> CanvasLayers => _projectManager.Project?.Layers.Reverse();

        public Layer TemporaryLayer => _projectManager.Project?.TemporaryLayer;

        public int Width => _projectManager.Project?.Width ?? 0;
        public int Height => _projectManager.Project?.Height ?? 0;

        [Notify(nameof(Zoom), nameof(ZoomFactor))]
        public int Zoom { get; set; } = 100;

        public float ZoomFactor => Zoom / 100.0f;

        public ICommand MouseMoveCommand { get; private set; }
        public ICommand MouseDownCommand { get; private set; }
        public ICommand MouseUpCommand { get; private set; }
        public ICommand MouseEnterCommand { get; private set; }
        public ICommand MouseLeaveCommand { get; private set; }
        public ICommand ResetZoomCommand { get; private set; }

        public void Dispose()
        {
            _layersChangedObserver?.Dispose();
        }

        private void InitializeCommands()
        {
            MouseMoveCommand = new RelayCommand<MouseEventArgs>(OnMouseMove);
            MouseDownCommand = new RelayCommand<MouseEventArgs>(OnMouseDown);
            MouseUpCommand = new RelayCommand<MouseEventArgs>(OnMouseUp);
            MouseEnterCommand = new RelayCommand<EventArgs>(OnMouseEnter);
            MouseLeaveCommand = new RelayCommand<EventArgs>(OnMouseLeave);
            ResetZoomCommand = new RelayCommand(() => Zoom = 100);
        }

        private void OnMouseMove(MouseEventArgs e)
        {
            _canvas.NotifyMouseMove(new MouseWithLocationEventArgs(e.Source as IInputElement, e));
        }

        private void OnMouseDown(MouseEventArgs e)
        {
            _canvas.NotifyMouseDown(new MouseWithLocationEventArgs(e.Source as IInputElement, e));
        }

        private void OnMouseUp(MouseEventArgs e)
        {
            _canvas.NotifyMouseUp(new MouseWithLocationEventArgs(e.Source as IInputElement, e));
        }

        private void OnMouseEnter(EventArgs e)
        {
            _canvas.NotifyMouseEnter(e);
        }

        private void OnMouseLeave(EventArgs e)
        {
            _canvas.NotifyMouseLeave(e);
        }

        private void OnProjectLoadedChanged(bool loaded)
        {
            if (!loaded) return;
            IObservable<EventPattern<NotifyCollectionChangedEventArgs>> layersChangedObservable =
                Observable.FromEventPattern<NotifyCollectionChangedEventArgs>(_projectManager.Project.Layers,
                    nameof(_projectManager.Project.Layers.CollectionChanged));
            _layersChangedObserver = layersChangedObservable.Subscribe(args => RaisePropertyChanged(() => CanvasLayers));
            RaisePropertyChanged(string.Empty);
        }
    }
}