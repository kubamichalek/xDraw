﻿using System.Reflection;
using Autofac;
using GalaSoft.MvvmLight;
using Module = Autofac.Module;

namespace xDraw.UI.ViewModels
{
	public class ViewModelModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
				.Where(type => type.IsAssignableTo<ViewModelBase>())
				.Keyed<ViewModelBase>(type => type.Name)
				.InstancePerDependency();
		}
	}
}