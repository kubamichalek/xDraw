﻿using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;

namespace xDraw.UI.ViewModels
{
	public class SplashScreenViewModel : ViewModelBase
	{
		public SplashScreenViewModel()
		{
			InitializeCommands();
		}

		public ICommand SplashMove { get; set; }

		private void InitializeCommands()
		{
			SplashMove = new RelayCommand<Window>(OnSplashMove);
		}

		private static void OnSplashMove(Window window)
		{
			window.DragMove();
		}
	}
}