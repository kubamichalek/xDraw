﻿using Autofac;
using xDraw.Logic;
using xDraw.UI.Framework;
using xDraw.UI.ViewModels;
using xDraw.UI.Views;

namespace xDraw.UI
{
	public class AppBootstrap
	{
		public IContainer Build()
		{
			var builder = new ContainerBuilder();
			LoadFramework(builder);
			LoadLogic(builder);
			LoadViewModels(builder);
			LoadViews(builder);
			return builder.Build();
		}

		private static void LoadFramework(ContainerBuilder builder)
		{
			builder.RegisterModule<FrameworkModule>();
		}

		private static void LoadLogic(ContainerBuilder builder)
		{
			builder.RegisterModule<LogicModule>();
		}

		private static void LoadViewModels(ContainerBuilder builder)
		{
			builder.RegisterModule<ViewModelModule>();
		}

		private static void LoadViews(ContainerBuilder builder)
		{
			builder.RegisterModule<ViewsModule>();
		}
	}
}