﻿using System.Threading.Tasks;
using System.Windows;
using Autofac;
using xDraw.UI.Framework;
using xDraw.UI.Views.Main;

namespace xDraw.UI
{
	public partial class App
	{
		private static SplashWindow _splash;

		private async void App_OnStartup(object sender, StartupEventArgs e)
		{
			ShowSplashScreen();
			await StartBootstrapper();
		}

		private async Task StartBootstrapper()
		{
			await Task.Run(() =>
			{
				var bootstrapper = new AppBootstrap();
				IContainer container = bootstrapper.Build();
				Resources["Locator"] = container.Resolve<ViewModelLocator>();
				Current.Dispatcher.Invoke(() =>
				{
					var mainWindow = container.Resolve<IMainWindow>();
					mainWindow.Loaded += OnMainWindowLoaded;
					mainWindow.Show();
				});
			});
		}

		private static void OnMainWindowLoaded(object sender, RoutedEventArgs e)
		{
			HideSplashScreen();
		}

		private static void HideSplashScreen()
		{
			_splash.Close();
		}

		private static void ShowSplashScreen()
		{
			_splash = new SplashWindow();
			_splash.Show();
		}
	}
}