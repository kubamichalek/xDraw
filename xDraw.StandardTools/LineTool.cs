﻿using System.Windows.Input;
using System.Windows.Media;
using xDraw.Common.EventArgs;
using xDraw.GFX.Layers;
using xDraw.Logic.GfxCanvas;
using xDraw.Logic.Projects;
using xDraw.Logic.Tools;
using xDraw.StandardTools.LineToolControls;
using xDraw.UI.Framework.Dispatcher;
using Graphics = xDraw.GFX.Graphics;
using Point = System.Windows.Point;

namespace xDraw.StandardTools
{
    public class LineTool : BaseTool
    {
        private readonly IProjectManager _projectManager;
        private readonly ICanvas _canvas;
        private Layer _tempLayer;
        private ToolbarSettings _settings;
        private Point? _anchor;

        public LineTool(IToolsManager toolsManager, IProjectManager projectManager, ICanvas canvas, IGuiDispatcher guiDispatcher) : base(toolsManager)
        {
            _projectManager = projectManager;
            _canvas = canvas;
            guiDispatcher.Invoke(() =>
            {
                _settings = new ToolbarSettings
                {
                    Thickness = 1
                };
            });

            SettingsPanelControl = _settings;
        }

        public override string Tooltip => "Linia";
        public override ImageSource IconSource => null;


        public override void Activated()
        {
            base.Activated();
            _tempLayer = _projectManager.Project.TemporaryLayer;
            _canvas.MouseDown += OnMouseDown;
            _canvas.MouseUp += OnMouseUp;
            _canvas.MouseMove += OnMouseMove;
        }

        public override void Deactivated()
        {
            base.Deactivated();
            _canvas.MouseDown -= OnMouseDown;
            _canvas.MouseUp -= OnMouseUp;
            _canvas.MouseMove -= OnMouseMove;
        }

        private void OnMouseDown(object sender, MouseWithLocationEventArgs e)
        {
            _anchor = e.Position;
        }

        private void OnMouseMove(object sender, MouseWithLocationEventArgs e)
        {
            if (_anchor != null && e.LeftButton != MouseButtonState.Pressed)
            {
                OnMouseUp(sender, e);
            }

            if (_anchor == null)
            {
                return;
            }

            using (var g = new Graphics(_tempLayer.Bitmap))
            {
                g.Clear();
                g.DrawLine((Point)_anchor, e.Position, _settings.Thickness, _projectManager.Project.SelectedColor);
            }
        }

        private void OnMouseUp(object sender, MouseWithLocationEventArgs e)
        {
            _projectManager.Project.ActiveLayer.MergeWith(_tempLayer);
            using (var g = new Graphics(_tempLayer.Bitmap))
            {
                g.Clear();
            }

            _anchor = null;
        }
    }
}