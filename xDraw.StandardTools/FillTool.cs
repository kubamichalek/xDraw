﻿using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using xDraw.Common.EventArgs;
using xDraw.GFX;
using xDraw.GFX.Layers;
using xDraw.Logic.GfxCanvas;
using xDraw.Logic.Projects;
using xDraw.Logic.Tools;
using xDraw.StandardTools.FillToolControls;
using xDraw.UI.Framework.Dispatcher;

namespace xDraw.StandardTools
{
    public class FillTool : BaseTool
    {
        private readonly ICanvas _canvas;
        private readonly IProjectManager _projectManager;

        private bool _inProgress;
        private ToolbarSettings _settings;

        public FillTool(IToolsManager toolsManager, IProjectManager projectManager, ICanvas canvas, IGuiDispatcher guiDispatcher) : base(toolsManager)
        {
            _projectManager = projectManager;
            _canvas = canvas;
            guiDispatcher.Invoke(() =>
            {
                _settings = new ToolbarSettings
                {
                    Tolerance = 10
                };
            });

            SettingsPanelControl = _settings;
        }

        public override string Tooltip => "Wypełnij";
        public override ImageSource IconSource => null;

        public override void Activated()
        {
            base.Activated();
            if (!_projectManager.IsLoaded) return;
            _canvas.MouseMove += OnMouseMove;
            _canvas.MouseDown += OnMouseDown;
            _canvas.MouseUp += OnMouseUp;
        }

        public override void Deactivated()
        {
            base.Deactivated();
            _canvas.MouseMove -= OnMouseMove;
            _canvas.MouseDown -= OnMouseDown;
            _canvas.MouseUp -= OnMouseUp;
        }

        private void OnMouseUp(object sender, MouseWithLocationEventArgs e)
        {
            _inProgress = false;
        }

        private void OnMouseDown(object sender, MouseWithLocationEventArgs e)
        {
            _inProgress = true;
            OnMouseMove(sender, e);
        }

        private void OnMouseMove(object sender, MouseWithLocationEventArgs e)
        {
            if (e.LeftButton != MouseButtonState.Pressed)
            {
                OnMouseUp(sender, e);
            }
            if (_inProgress)
            {
                Fill(_projectManager.Project.ActiveLayer, e.Position);
            }
        }

        private void Fill(Layer layer, Point p)
        {
            using (var g = new Graphics(layer.Bitmap))
            {
                g.FloodFill(_projectManager.Project.SelectedColor, p, _settings.Tolerance * 50);
            }
        }
    }
}