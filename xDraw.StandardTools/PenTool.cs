﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using xDraw.Common.EventArgs;
using xDraw.GFX;
using xDraw.GFX.Layers;
using xDraw.Logic.GfxCanvas;
using xDraw.Logic.Projects;
using xDraw.Logic.Tools;

namespace xDraw.StandardTools
{
    public class PenTool : BaseTool
    {
        private readonly ICanvas _canvas;
        private readonly IProjectManager _projectManager;
        private Point? _lastPoint;

        public PenTool(IToolsManager toolsManager, IProjectManager projectManager, ICanvas canvas) : base(toolsManager)
        {
            _projectManager = projectManager;
            _canvas = canvas;
        }

        public override string Tooltip => "Ołówek";
        public override ImageSource IconSource => null;

        public override void Activated()
        {
            base.Activated();
            if (!_projectManager.IsLoaded) return;
            _canvas.MouseMove += OnMouseMove;
            _canvas.MouseDown += OnMouseDown;
            _canvas.MouseUp += OnMouseUp;
            _canvas.MouseLeave += OnMouseLeave;
        }

        public override void Deactivated()
        {
            base.Deactivated();
            _canvas.MouseMove -= OnMouseMove;
            _canvas.MouseDown -= OnMouseDown;
            _canvas.MouseUp -= OnMouseUp;
            _canvas.MouseLeave -= OnMouseLeave;
        }

        private void OnMouseUp(object sender, MouseWithLocationEventArgs e)
        {
            _lastPoint = null;
        }

        private void OnMouseDown(object sender, MouseWithLocationEventArgs e)
        {
            _lastPoint = e.Position;
        }

        private void OnMouseMove(object sender, MouseWithLocationEventArgs e)
        {
            if (e.LeftButton != MouseButtonState.Pressed) return;
            if (_lastPoint != null)
            {
                PerformDrawing(_projectManager.Project.ActiveLayer, _lastPoint.Value, e.Position);
            }
            _lastPoint = e.Position;
        }

        private void OnMouseLeave(object sender, EventArgs e)
        {
            _lastPoint = null;
        }

        private void PerformDrawing(Layer layer, Point p1, Point p2)
        {
            using (var g = new Graphics(layer.Bitmap))
            {
                g.DrawPen(_projectManager.Project.SelectedColor, p1, p2);
            }
        }
    }
}