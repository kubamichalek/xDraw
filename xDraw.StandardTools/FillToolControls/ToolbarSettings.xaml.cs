﻿namespace xDraw.StandardTools.FillToolControls
{
    public partial class ToolbarSettings
    {
        public ToolbarSettings()
        {
            InitializeComponent();
            DataContext = this;
        }

        public int Tolerance { get; set; }
    }
}
