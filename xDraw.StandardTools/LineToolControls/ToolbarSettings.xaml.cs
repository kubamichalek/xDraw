﻿namespace xDraw.StandardTools.LineToolControls
{
    public partial class ToolbarSettings
    {
        public ToolbarSettings()
        {
            InitializeComponent();
            DataContext = this;
        }

        public int Thickness { get; set; }
    }
}
