﻿using System.Windows.Media;
using xDraw.Logic.Tools;

namespace xDraw.StandardTools
{
    public class MouseTool : BaseTool
    {
        public MouseTool(IToolsManager toolsManager) : base(toolsManager)
        {
            Order = 0;
        }

        public override string Tooltip => "Mysz";
        public override ImageSource IconSource => null;
    }
}