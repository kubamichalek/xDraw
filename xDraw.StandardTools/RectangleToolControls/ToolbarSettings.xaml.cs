﻿namespace xDraw.StandardTools.RectangleToolControls
{
    public partial class ToolbarSettings
    {
        public ToolbarSettings()
        {
            InitializeComponent();
            DataContext = this;
        }

        public int Thickness { get; set; }
    }
}
