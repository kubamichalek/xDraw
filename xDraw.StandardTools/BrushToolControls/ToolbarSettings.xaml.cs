﻿using System.Collections.Generic;
using System.Linq;
using xDraw.Logic.Brushes;

namespace xDraw.StandardTools.BrushToolControls
{
    public partial class ToolbarSettings
    {
        private BrushModel _selectedBrush;
        private int _size;

        public ToolbarSettings(IList<BrushModel> brushes)
        {
            InitializeComponent();
            DataContext = this;
            Brushes = brushes;
            SelectedBrush = Brushes.FirstOrDefault();
            Size = 10;
        }

        public IList<BrushModel> Brushes { get; }

        public BrushModel SelectedBrush
        {
            get => _selectedBrush;
            set
            {
                _selectedBrush = value;
                _selectedBrush.Size = Size;
            }
        }

        public int Size
        {
            get => _size;
            set
            {
                if (SelectedBrush == null) return;
                SelectedBrush.Size = value;
                _size = value;
            }
        }
    }
}
