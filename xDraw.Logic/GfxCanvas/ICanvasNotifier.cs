﻿using System;
using xDraw.Common.EventArgs;

namespace xDraw.Logic.GfxCanvas
{
	public interface ICanvasNotifier
	{
		void NotifyMouseMove(MouseWithLocationEventArgs args);
		void NotifyMouseUp(MouseWithLocationEventArgs args);
		void NotifyMouseDown(MouseWithLocationEventArgs args);
		void NotifyMouseEnter(EventArgs args);
		void NotifyMouseLeave(EventArgs args);
	}
}