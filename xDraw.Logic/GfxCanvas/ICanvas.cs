﻿using System;
using xDraw.Common.EventArgs;

namespace xDraw.Logic.GfxCanvas
{
	public interface ICanvas
	{
		event MouseWithLocaionEventHandler MouseMove;
		event MouseWithLocaionEventHandler MouseUp;
		event MouseWithLocaionEventHandler MouseDown;
		event EventHandler MouseLeave;
		event EventHandler MouseEnter;
	}
}