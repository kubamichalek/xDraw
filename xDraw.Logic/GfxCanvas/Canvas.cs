﻿using System;
using xDraw.Common.EventArgs;

namespace xDraw.Logic.GfxCanvas
{
	public class Canvas : ICanvas, ICanvasNotifier
	{
		public event MouseWithLocaionEventHandler MouseMove;
		public event MouseWithLocaionEventHandler MouseUp;
		public event MouseWithLocaionEventHandler MouseDown;
		public event EventHandler MouseLeave;
		public event EventHandler MouseEnter;

		public void NotifyMouseMove(MouseWithLocationEventArgs args)
		{
			MouseMove?.Invoke(this, args);
		}

		public void NotifyMouseUp(MouseWithLocationEventArgs args)
		{
			MouseUp?.Invoke(this, args);
		}

		public void NotifyMouseDown(MouseWithLocationEventArgs args)
		{
			MouseDown?.Invoke(this, args);
		}

		public void NotifyMouseEnter(EventArgs args)
		{
			MouseEnter?.Invoke(this, args);
		}

		public void NotifyMouseLeave(EventArgs args)
		{
			MouseLeave?.Invoke(this, args);
		}
	}
}