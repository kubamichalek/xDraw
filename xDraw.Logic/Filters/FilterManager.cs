﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Media.Imaging;

namespace xDraw.Logic.Filters
{
    internal class FilterManager : IFilterManager
    {
        private readonly List<IFilter> _filters = new List<IFilter>();

        public IList<IFilter> Filters => _filters.ToList();

        public void Register(IFilter filter)
        {
            _filters.Add(filter);
        }

        public unsafe void Start(IFilter filter, WriteableBitmap bitmap)
        {
            using (BitmapContext ctx = bitmap.GetBitmapContext())
            {
                filter.Execute(ctx.Pixels, new Size(ctx.Width, ctx.Height), ctx.Format);
            }
        }
    }
}
