﻿using System.Collections.Generic;
using System.Windows.Media.Imaging;

namespace xDraw.Logic.Filters
{
    public interface IFilterManager
    {
        IList<IFilter> Filters { get; }

        void Register(IFilter filter);
        void Start(IFilter filter, WriteableBitmap bitmap);
    }
}