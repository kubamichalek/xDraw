﻿using System.Windows;
using System.Windows.Media;

namespace xDraw.Logic.Filters
{
    public interface IFilter
    {
        string Name { get; }

        unsafe void Execute(int* pixels, Size size, PixelFormat format);
    }
}