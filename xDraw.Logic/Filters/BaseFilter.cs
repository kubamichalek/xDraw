﻿using System.Windows;
using System.Windows.Media;

namespace xDraw.Logic.Filters
{
	public abstract class BaseFilter : IFilter
	{
		protected BaseFilter(IFilterManager filterManager)
		{
			filterManager.Register(this);
		}

		public abstract string Name { get; }
		public abstract unsafe void Execute(int* pixels, Size size, PixelFormat format);
	}
}