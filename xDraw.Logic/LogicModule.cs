﻿using System;
using System.IO;
using System.Reflection;
using Autofac;
using xDraw.GFX.Layers;
using xDraw.Logic.Brushes;
using xDraw.Logic.Brushes.Implementations;
using xDraw.Logic.Files;
using xDraw.Logic.Filters;
using xDraw.Logic.GfxCanvas;
using xDraw.Logic.Projects;
using xDraw.Logic.Projects.Models;
using xDraw.Logic.Tools;
using Module = Autofac.Module;

namespace xDraw.Logic
{
	public class LogicModule : Module
	{
		private const string PLUGINS_DIRECTORY = "Plugins";

		protected override void Load(ContainerBuilder builder)
		{
			builder.RegisterType<LayerContainer>().As<ILayerContainer>().SingleInstance();
			builder.RegisterType<ProjectManager>().As<IProjectManager>().SingleInstance();
			builder.RegisterType<Canvas>().As<ICanvas>().As<ICanvasNotifier>().SingleInstance();
			builder.RegisterType<NewProjectModel>().AsSelf().SingleInstance();
			builder.RegisterModule<FilesModule>();
			builder.RegisterType<ZipArchiveBrushesRepository>().WithParameter("fileName", "brushes/brushes.zip").As<IBrushesRepository>().SingleInstance();
			builder.RegisterType<ToolsManager>().As<IToolsManager>().SingleInstance();
			builder.RegisterType<FilterManager>().As<IFilterManager>().SingleInstance();
			RegisterPlugins(builder);
		}

		private static void RegisterPlugins(ContainerBuilder builder)
		{
			if (Directory.Exists(PLUGINS_DIRECTORY))
			{
				foreach (string file in Directory.EnumerateFiles(PLUGINS_DIRECTORY))
				{
					if (Path.GetExtension(file) == ".dll")
					{
						try
						{
							Assembly pluginAssembly =
								Assembly.LoadFile(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), file));

							RegisterToolsInAssembly(builder, pluginAssembly);
							RegisterFiltersInAssembly(builder, pluginAssembly);
						}
						catch (BadImageFormatException)
						{
							// Not a managed library, go on...
						}
					}
				}
			}
		}

		private static void RegisterFiltersInAssembly(ContainerBuilder builder, Assembly pluginAssembly)
		{
			builder.RegisterAssemblyTypes(pluginAssembly)
				.AssignableTo<BaseFilter>().AutoActivate();
		}

		private static void RegisterToolsInAssembly(ContainerBuilder builder, Assembly pluginAssembly)
		{
			builder.RegisterAssemblyTypes(pluginAssembly)
				.AssignableTo<BaseTool>().AutoActivate();
		}
	}
}