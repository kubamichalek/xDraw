﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Autofac;
using Autofac.Builder;
using Module = Autofac.Module;

namespace xDraw.Logic.Files
{
    internal class FilesModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<StandardFileHandler>().As<IFileHandler>().SingleInstance();

            RegisterFileParsers(builder);
            RegisterFileSavers(builder);
        }

        private static void RegisterFileParsers(ContainerBuilder builder)
        {
            RegisterKeyedTypesSupporingFormats<IFileParser>(builder);
        }

        private static void RegisterFileSavers(ContainerBuilder builder)
        {
            RegisterKeyedTypesSupporingFormats<IFileSaver>(builder);
        }

        private static void RegisterKeyedTypesSupporingFormats<T>(ContainerBuilder builder) where T : class
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            foreach (Type type in assembly.GetTypes().Where(t => t.IsAssignableTo<T>() && t.GetCustomAttributes<SupportedFormatAttribute>().Any()))
            {
                IRegistrationBuilder<object, ConcreteReflectionActivatorData, SingleRegistrationStyle>
                    registrationBuilder = builder.RegisterType(type).As<T>();

                IEnumerable<SupportedFormatAttribute> supportedFormatAttributes = type.GetCustomAttributes<SupportedFormatAttribute>();
                foreach (SupportedFormatAttribute attr in supportedFormatAttributes)
                {
                    registrationBuilder.Keyed<T>(attr.Extension);
                }
            }
        }
    }
}