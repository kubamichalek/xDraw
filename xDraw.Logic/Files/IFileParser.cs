﻿using xDraw.Logic.Projects;

namespace xDraw.Logic.Files
{
    public interface IFileParser
    {
        void Initialize(string filePath);
        Project CreateProject();
    }
}