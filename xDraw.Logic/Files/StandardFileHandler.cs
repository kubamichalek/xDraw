﻿using System.IO;
using Autofac.Features.Indexed;

namespace xDraw.Logic.Files
{
    public class StandardFileHandler : IFileHandler
    {
        private readonly IIndex<string, IFileParser> _fileParsers;
        private readonly IIndex<string, IFileSaver> _fileSavers;

        public StandardFileHandler(IIndex<string, IFileParser> fileParsers, IIndex<string, IFileSaver> fileSavers)
        {
            _fileParsers = fileParsers;
            _fileSavers = fileSavers;
        }

        public bool CanOpenFile(string filePath)
        {
            string extension = ExtractExtension(filePath);
            return _fileParsers.TryGetValue(extension, out IFileParser _);
        }

        public bool CanSaveFile(string filePath)
        {
            string extension = ExtractExtension(filePath);
            return _fileSavers.TryGetValue(extension, out IFileSaver _);
        }

        public IFileParser CreateParserForFile(string filePath)
        {
            string extension = ExtractExtension(filePath);
            IFileParser fileParser = _fileParsers[extension];
            fileParser.Initialize(filePath);
            return fileParser;
        }

        public IFileSaver CreateSaverForFile(string filePath)
        {
            string extension = ExtractExtension(filePath);
            IFileSaver fileSaver = _fileSavers[extension];
            fileSaver.Initialize(filePath);
            return fileSaver;
        }

        private static string ExtractExtension(string filePath)
        {
            return Path.GetExtension(filePath);
        }
    }
}