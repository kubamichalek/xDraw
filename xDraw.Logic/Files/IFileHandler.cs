﻿namespace xDraw.Logic.Files
{
    public interface IFileHandler
    {
        bool CanOpenFile(string filePath);
        bool CanSaveFile(string filePath);
        IFileParser CreateParserForFile(string filePath);
        IFileSaver CreateSaverForFile(string filePath);
    }
}