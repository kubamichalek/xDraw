﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using xDraw.Logic.Projects;

namespace xDraw.Logic.Files.FileParsers
{
    [SupportedFormat(".jpg")]
    [SupportedFormat(".jpeg")]
    [SupportedFormat(".png")]
    [SupportedFormat(".bmp")]
    public class StandardPlainImageParser : IFileParser
    {
        private string _filePath;

        public void Initialize(string filePath)
        {
            _filePath = filePath;
        }

        public Project CreateProject()
        {
            var source = new BitmapImage(new Uri(_filePath));
            int stride = source.PixelWidth * (source.Format.BitsPerPixel + 7) / 8;
            var buffer = new byte[stride * source.PixelHeight];
            source.CopyPixels(buffer, stride, 0);
            string fileName = Path.GetFileName(_filePath);
            var project = new Project(fileName, source.PixelWidth, source.PixelHeight, Colors.Transparent);
            project.ActiveLayer.Bitmap.WritePixels(new Int32Rect(0, 0, source.PixelWidth, source.PixelHeight), buffer, stride, 0);
            return project;
        }
    }
}