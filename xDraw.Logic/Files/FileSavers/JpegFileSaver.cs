﻿using System.IO;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using xDraw.Logic.Projects;

namespace xDraw.Logic.Files.FileSavers
{
    [SupportedFormat(".jpg")]
    [SupportedFormat(".jpeg")]
    public class JpegFileSaver : IFileSaver
    {
        private string _filePath;

        public void Initialize(string filePath)
        {
            _filePath = filePath;
        }

        public void SaveProject(Project project)
        {
            WriteableBitmap bitmap = project.GetPlainLayer(Colors.White).Bitmap;
            var encoder = new JpegBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(bitmap));
            using (FileStream stream = File.OpenWrite(_filePath))
            {
                encoder.Save(stream);
            }
        }
    }
}