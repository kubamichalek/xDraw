﻿using System.IO;
using System.Windows.Media.Imaging;
using xDraw.Logic.Projects;

namespace xDraw.Logic.Files.FileSavers
{
    [SupportedFormat(".png")]
    public class PngFileSaver : IFileSaver
    {
        private string _filePath;

        public void Initialize(string filePath)
        {
            _filePath = filePath;
        }

        public void SaveProject(Project project)
        {
            WriteableBitmap bitmap = project.GetPlainLayer().Bitmap;
            var encoder = new PngBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(bitmap));
            using (FileStream stream = File.OpenWrite(_filePath))
            {
                encoder.Save(stream);
            }
        }
    }
}