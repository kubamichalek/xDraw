﻿using System;

namespace xDraw.Logic.Files
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class SupportedFormatAttribute : Attribute
    {
        public string Extension { get; }

        public SupportedFormatAttribute(string extension)
        {
            Extension = extension;
        }
    }
}