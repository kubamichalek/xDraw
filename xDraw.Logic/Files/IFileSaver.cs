﻿using xDraw.Logic.Projects;

namespace xDraw.Logic.Files
{
    public interface IFileSaver
    {
        void Initialize(string filePath);
        void SaveProject(Project project);
    }
}