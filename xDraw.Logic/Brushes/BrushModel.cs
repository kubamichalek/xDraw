﻿using System.Windows.Media.Imaging;

namespace xDraw.Logic.Brushes
{
	public class BrushModel
	{
		private int _size;
		private readonly WriteableBitmap _image;

		public BrushModel(WriteableBitmap writeableBitmap)
		{
			_image = writeableBitmap;
			Image = _image;
		}

		public int Size
		{
			get => _size;
			set
			{
				if (_size == value) return;
				_size = value;
				ResizeImage();
			}
		}

		public WriteableBitmap Image { get; set; }

		private void ResizeImage()
		{
			Image = _image.Resize(_size * 3, _size * 3, WriteableBitmapExtensions.Interpolation.Bilinear);
		}
	}
}