﻿using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Windows.Media.Imaging;

namespace xDraw.Logic.Brushes.Implementations
{
	public class ZipArchiveBrushesRepository : IBrushesRepository
	{
		private readonly IReadOnlyCollection<ZipArchiveEntry> _files;

		public ZipArchiveBrushesRepository(string fileName)
		{
			ZipArchive zip = ZipFile.OpenRead(fileName);
			_files = zip.Entries;
		}

		public IList<BrushModel> GetBrushes()
		{
			return _files.Select(source => new BrushModel(BitmapFactory.ConvertToPbgra32Format(CreateBitmapImageFromZip(source))))
				.ToList();
		}

		private static BitmapImage CreateBitmapImageFromZip(ZipArchiveEntry brush)
		{
			using (Stream zipStream = brush.Open())
			using (var stream = new MemoryStream())
			{
				zipStream.CopyTo(stream);
				var bitmap = new BitmapImage();
				bitmap.BeginInit();
				bitmap.CacheOption = BitmapCacheOption.OnLoad;
				bitmap.StreamSource = stream;
				bitmap.EndInit();
				bitmap.Freeze();
				return bitmap;
			}
		}
	}
}