using System.Collections.Generic;

namespace xDraw.Logic.Brushes
{
	public interface IBrushesRepository
	{
		IList<BrushModel> GetBrushes();
	}
}