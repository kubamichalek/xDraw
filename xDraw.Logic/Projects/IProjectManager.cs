﻿using System;

namespace xDraw.Logic.Projects
{
    public interface IProjectManager
    {
        IObservable<bool> Loaded { get; }
        bool IsLoaded { get; }
        Project Project { get; }
        void CreateNewProject();
        void LoadProjectFromFile(string filePath);
        void SaveCurrentProjectToFile(string filePath);
    }
}