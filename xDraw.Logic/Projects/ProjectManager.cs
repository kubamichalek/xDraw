﻿using System;
using System.Linq;
using System.Reactive.Subjects;
using xDraw.Common.Modals;
using xDraw.Logic.Files;
using xDraw.Logic.Projects.Models;
using xDraw.Logic.Tools;

namespace xDraw.Logic.Projects
{
    public class ProjectManager : IProjectManager
    {
        private readonly NewProjectModel _newProjectModel;
        private readonly IToolsManager _toolsManager;
        private readonly IFileHandler _fileHandler;
        private readonly Subject<bool> _loaded = new Subject<bool>();
        private bool _isLoaded;

        public IObservable<bool> Loaded => _loaded;

        public bool IsLoaded
        {
            get => _isLoaded;
            private set
            {
                _isLoaded = value;
                _loaded.OnNext(value);
            }
        }

        public ProjectManager(NewProjectModel newProjectModel, IToolsManager toolsManager, IFileHandler fileHandler)
        {
            _newProjectModel = newProjectModel;
            _toolsManager = toolsManager;
            _fileHandler = fileHandler;
        }

        public Project Project { get; private set; }

        public void CreateNewProject()
        {
            var dialog = new NewProjectModal();
            if (!dialog.ShowDialog().GetValueOrDefault()) return;
            Project?.Unload();
            IsLoaded = false;
            Project = new Project(_newProjectModel.ProjectName, _newProjectModel.CanvasWidth, _newProjectModel.CanvasHeight,
                _newProjectModel.CanvasBackgroundColor);
            IsLoaded = true;
            _toolsManager.CurrentTool = _toolsManager.AvailableTools.FirstOrDefault();
        }

        public void LoadProjectFromFile(string filePath)
        {
            if (!_fileHandler.CanOpenFile(filePath))
            {
                throw new ArgumentException("Ten typ pliku nie jest wspierany", nameof(filePath));
            }

            IFileParser fileParser = _fileHandler.CreateParserForFile(filePath);
            Project?.Unload();
            IsLoaded = false;
            Project = fileParser.CreateProject();
            IsLoaded = true;
        }

        public void SaveCurrentProjectToFile(string filePath)
        {
            if (!_fileHandler.CanSaveFile(filePath))
            {
                throw new ArgumentException("Ten typ pliku nie jest wspierany", nameof(filePath));
            }

            IFileSaver fileSaver = _fileHandler.CreateSaverForFile(filePath);
            fileSaver.SaveProject(Project);
        }
    }
}