using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Media;
using Mathtone.MIST;
using xDraw.Common;
using xDraw.GFX;
using xDraw.GFX.Layers;

namespace xDraw.Logic.Projects
{
    [Notifier]
    public class Project : NotifierBase
    {
        public Project(string name, int width, int height, Color canvasBackgroundColor)
        {
            Name = name;
            Width = width;
            Height = height;
            Layers = new ObservableCollection<Layer>();
            CreateBackgroundLayer(canvasBackgroundColor);
            ActiveLayer = Layers[0];
            SelectedColor = Colors.Black;
            TemporaryLayer = new Layer(width, height, Colors.Transparent, "Temporary");
        }

        public string Name { get; }
        public bool IsFileBased { get; }

        public int Width { get; }

        public int Height { get; }

        public ObservableCollection<Layer> Layers { get; }

        [Notify]
        public Layer ActiveLayer { get; set; }

        public Layer TemporaryLayer { get; }

        [Notify]
        public Color SelectedColor { get; set; }

        private void CreateBackgroundLayer(Color canvasBackgroundColor)
        {
            var layer = new Layer(Width, Height, canvasBackgroundColor, "T�o");
            Layers.Add(layer);
        }

        public void Unload()
        {
            ActiveLayer = null;
            Layers.Clear();
        }

        public void CreateNewLayer()
        {
            var layer = new Layer(Width, Height, Colors.Transparent, "Nowa warstwa");
            Layers.Insert(0, layer);
        }

        public void DeleteActiveLayer()
        {
            Layers.Remove(ActiveLayer);
            ActiveLayer = Layers[0];
        }

        public void MergeLayers()
        {
            Layer plain = GetPlainLayer();
            Layers.Clear();
            Layers.Add(plain);
            ActiveLayer = plain;
        }

        public Layer GetPlainLayer(Color? bgColor = null)
        {
            var layer = new Layer(Width, Height, bgColor ?? Colors.Transparent, "Scalona");
            using (var gfx = new Graphics(layer.Bitmap))
            {
                gfx.FillWithBitmap(Graphics.MergeBitmaps(Layers.Select(l => l.Bitmap)));
            }
            return layer;
        }

        public void Save()
        {

        }
    }
}