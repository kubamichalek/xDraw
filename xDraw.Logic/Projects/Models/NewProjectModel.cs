﻿using System.Windows.Media;

namespace xDraw.Logic.Projects.Models
{
	public class NewProjectModel
	{
		public string ProjectName { get; set; }
		public int CanvasWidth { get; set; }
		public int CanvasHeight { get; set; }
		public Color CanvasBackgroundColor { get; set; }
	}
}