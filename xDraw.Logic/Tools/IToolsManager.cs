﻿using System;
using System.Collections.Generic;
using System.Reactive;

namespace xDraw.Logic.Tools
{
    public interface IToolsManager
    {
        IObservable<EventPattern<object>> ToolChanged { get; }

        IList<ITool> AvailableTools { get; }
        ITool CurrentTool { get; set; }

        void Register(ITool tool);
    }
}