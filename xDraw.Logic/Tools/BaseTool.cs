﻿using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using GalaSoft.MvvmLight.Command;
using Mathtone.MIST;
using xDraw.Common;

namespace xDraw.Logic.Tools
{
    [Notifier]
    public abstract class BaseTool : NotifierBase, ITool
    {
        private readonly IToolsManager _toolsManager;

        protected BaseTool(IToolsManager toolsManager)
        {
            _toolsManager = toolsManager;
            toolsManager.Register(this);

            InitializeCommands();
        }

        public abstract string Tooltip { get; }
        public int Order { get; protected set; } = 100;
        public abstract ImageSource IconSource { get; }
        public Control SettingsPanelControl { get; protected set; }
        public ICommand ActivateCommand { get; private set; } //TODO: Review.

        [Notify]
        public bool IsSelected { get; private set; }


        private void InitializeCommands()
        {
            ActivateCommand = new RelayCommand(OnActivateCommand);
        }

        private void OnActivateCommand()
        {
            _toolsManager.CurrentTool = this;
        }

        public virtual void Activated()
        {
            IsSelected = true;
        }

        public virtual void Deactivated()
        {
            IsSelected = false;
        }
    }
}