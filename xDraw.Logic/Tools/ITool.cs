using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace xDraw.Logic.Tools
{
    public interface ITool
    {
        string Tooltip { get; }
        int Order { get; }
        ImageSource IconSource { get; }
        Control SettingsPanelControl { get; }
        ICommand ActivateCommand { get; }
        bool IsSelected { get; }
        void Activated();
        void Deactivated();
    }
}