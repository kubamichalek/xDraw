﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Reactive.Linq;

namespace xDraw.Logic.Tools
{
    public class ToolsManager : IToolsManager
    {
        private ITool _currentTool;
        private readonly List<ITool> _availableTools;

        private event EventHandler _toolChanged;


        public ToolsManager()
        {
            _availableTools = new List<ITool>();
            ToolChanged = Observable.FromEventPattern(handler => _toolChanged += handler,
                handler => _toolChanged -= handler);
        }

        public ITool CurrentTool
        {
            get => _currentTool;
            set
            {
                if (_currentTool == value)
                    return;
                NotifyAboutDeactivation();
                _currentTool = value;
                NotifyAboutActivation();
                _toolChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        public IObservable<EventPattern<object>> ToolChanged { get; }

        public IList<ITool> AvailableTools => _availableTools.OrderBy(o => o.Order).ToList();

        private void NotifyAboutActivation()
        {
            _currentTool?.Activated();
        }

        private void NotifyAboutDeactivation()
        {
            _currentTool?.Deactivated();
        }

        public void Register(ITool tool)
        {
            _availableTools.Add(tool);
        }
    }
}