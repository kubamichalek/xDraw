﻿using System;
using System.Windows;

namespace xDraw.UI.Framework.Dispatcher.Implementation
{
	public class GuiDispatcher : IGuiDispatcher
	{
		public void Invoke(Action a)
		{
			Application.Current.Dispatcher.Invoke(a);
		}
	}
}