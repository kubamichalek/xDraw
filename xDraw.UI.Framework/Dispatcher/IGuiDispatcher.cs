﻿using System;

namespace xDraw.UI.Framework.Dispatcher
{
	public interface IGuiDispatcher
	{
		void Invoke(Action a);
	}
}