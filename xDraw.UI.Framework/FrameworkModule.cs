﻿using Autofac;
using xDraw.UI.Framework.Dispatcher;
using xDraw.UI.Framework.Dispatcher.Implementation;

namespace xDraw.UI.Framework
{
	public class FrameworkModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			builder.RegisterType<ViewModelLocator>();
			builder.RegisterType<GuiDispatcher>().As<IGuiDispatcher>().SingleInstance();
		}
	}
}