﻿using System.Windows;

namespace xDraw.UI.Framework
{
	public interface IMainWindow
	{
		event RoutedEventHandler Loaded;
		void Show();
	}
}