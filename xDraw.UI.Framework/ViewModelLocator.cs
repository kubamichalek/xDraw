﻿using System.Dynamic;
using Autofac.Features.Indexed;
using GalaSoft.MvvmLight;

namespace xDraw.UI.Framework
{
    public class ViewModelLocator : DynamicObject
    {
        private readonly IIndex<string, ViewModelBase> _viewModels;

        public ViewModelLocator(IIndex<string, ViewModelBase> viewModels)
        {
            _viewModels = viewModels;
        }

        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            if (_viewModels.TryGetValue(binder.Name, out ViewModelBase viewModel))
            {
                result = viewModel;
                return true;
            }

            result = null;
            return false;
        }
    }
}